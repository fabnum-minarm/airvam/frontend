const {defineConfig} = require("cypress");
const {isFileExist} = require('cy-verify-downloads');
require('dotenv').config();

module.exports = defineConfig({
  projectId: "",
  numTestsKeptInMemory: 5,
  experimentalMemoryManagement: true,
  defaultCommandTimeout: 10000,

  env: {
    devTestUser: "dev-user@email.com",
    devTestAdmin: "dev-admin@email.com",
    devTestGod: "dev-super-admin@email.com",
    apiUrl: process.env.API_URL
  },

  e2e: {
    baseUrl: "http://localhost:4200",
    setupNodeEvents(on, config) {
      require('@cypress/code-coverage/task')(on, config);
      on('task', {isFileExist});
      return config;
    },
  },

  viewportHeight: 800,
  viewportWidth: 1400
});

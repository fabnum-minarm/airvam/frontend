export default defineNuxtRouteMiddleware((to) => {
  if (!to.path.includes('pax') && !to.path.includes('animal') && !to.path.includes('plan_cabine')) {
    return navigateTo(`${to.path}/pax`);
  }
})

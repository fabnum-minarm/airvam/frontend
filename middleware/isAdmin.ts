export default defineNuxtRouteMiddleware(() => {
  const store = useStore();

  if (!store.loggedInUser || !['admin', 'god'].includes(store.loggedInUser?.role)) {
    return navigateTo('/');
  }
})


export default defineNuxtRouteMiddleware((to) => {
  if (!to.path.includes('edition') && !to.path.includes('dossier')) {
    return navigateTo(`${to.path}/edition`);
  }
})

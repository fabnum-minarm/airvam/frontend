import { Leg } from "~/dto/leg.dto";

export class LegDocument {
  id: number;
  leg: Leg;
  paxManifest: string;
  animalManifest: string;
  paxListing: string;
  tm5: string;
  underage: string;
  pnl: string;
  cover: string;
}

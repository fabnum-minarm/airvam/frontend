import { SeatZone } from "~/dto/seat-zone.enum";

export class SeatTemplate {
  id: number;
  row: string;
  number: number;
  zone: SeatZone;
}

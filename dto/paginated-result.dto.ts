export class PaginatedResult<T> {
  meta: {
    currentPage: number;
    totalPages: number;
    totalItems: number;
    itemCount: number;
    last: boolean;
  }
  items: T[];

  constructor() {
    this.meta = {
      currentPage: 1,
      totalPages: 0,
      totalItems: 0,
      itemCount: 0,
      last: false
    };
    this.items = [];
  }

}

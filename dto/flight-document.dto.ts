export type FlightDocumentFile = {
  id: number;
  filename: string;
}

export class FlightDocument {
  id: number;
  paxManifest: FlightDocumentFile;
  animalManifest: FlightDocumentFile;
  paxListing: FlightDocumentFile;
  tm5: FlightDocumentFile;
  underage: FlightDocumentFile;
  pnl: FlightDocumentFile;
  cover: FlightDocumentFile;
}

export enum SeatZone {
  OA = 'OA',
  OB = 'OB',
  OC = 'OC'
}

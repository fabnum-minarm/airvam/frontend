export enum LegStatus {
  to_schedule = 'À préparer',
  incomplete = 'Incomplet',
  scheduled = 'Préparé',
  lmc = 'LMC',
  finalized = 'Finalisé',
  archived = 'Archivé'
}
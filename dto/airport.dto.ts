export class Airport {
  id: number;
  oaci: string;
  iata: string;
  name: string;
  ticketMention: string;
  timezone: string;
  disabled: boolean;
}

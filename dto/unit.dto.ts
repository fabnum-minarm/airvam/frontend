import { Airport } from "~/dto/airport.dto";

export class Unit {
  id: number;
  name: string;
  code: string;
  address1: string;
  address2: string;
  address3: string;
  phoneNumber: string;
  email: string;
  airports: Airport[];
  disabled: boolean;
}

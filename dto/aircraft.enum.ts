export enum AircraftEnum {
  '40_144_88' = 'A330 40-144-88',
  'F-UJCS' = 'A330 F-UJCS',
  'F-UJCT' = 'A330 F-UJCT',
  'F-UJCU' = 'A330 F-UJCU',
  '88Y' = 'A330 88Y',
}

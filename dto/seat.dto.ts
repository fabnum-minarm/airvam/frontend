import { SeatTemplate } from "~/dto/seat-template.dto";
import { Pax } from "~/dto/pax.dto";

export class Seat extends SeatTemplate {
  comment: string;
  observations: string;
  pax?: Pax;
  blocked: boolean;
}

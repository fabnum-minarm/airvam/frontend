import { Rank } from "~/dto/rank.dto";
import { Unit } from "~/dto/unit.dto";
import type { UserRole } from "./userRole.enum";

export class User {
  email: string;
  firstName: string;
  lastName: string;
  rank: Rank;
  role: keyof typeof UserRole;
  unit: Unit;
  disabled: boolean;
}

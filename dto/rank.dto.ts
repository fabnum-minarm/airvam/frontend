export class Rank {
  id: number;
  rank: number;
  name: string;
  category: string;
  disabled: boolean;
}

import { Rank } from "~/dto/rank.dto";
import { Leg } from "~/dto/leg.dto";
import { Airport } from "~/dto/airport.dto";
import { Flight } from "~/dto/flight.dto";
import { Animal } from "~/dto/animal.dto";

export class Pax {
  id: number;
  atr: number;
  departure: Airport;
  arrival: Airport;
  number: number;
  weight: number;
  luggageWeight: number;
  gender: string;
  lastName: string;
  firstName: string;
  rank: Rank;
  category: string;
  militaryId: string;
  birthdate: string;
  unit: string;
  nationality: string;
  observations: string;
  underage: boolean;
  legs: Leg[];
  flight: Flight;
  animals: Animal[];

  // Only FRONTEND
  isDuplicate: boolean;
  hasError: boolean;
}

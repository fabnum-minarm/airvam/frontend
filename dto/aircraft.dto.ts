import { SeatTemplate } from "~/dto/seat-template.dto";

export class Aircraft {
  name: string;
  shortName: string;
  seatsTemplate: SeatTemplate[];
}

export enum RankCategory {
  'Air et Espace',
  'Terre',
  'Marine',
  'OTAN',
  'Gendarmerie',
  'SSA',
  'Commissaire',
  'Autre',
}

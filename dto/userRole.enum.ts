export enum UserRole {
  user = 'Utilisateur',
  admin = 'Administrateur',
  god = 'Super Administrateur',
}

import { Pax } from "~/dto/pax.dto";
import { Rank } from "~/dto/rank.dto";

export class Animal {
  id: number;
  animalName: string;
  animalId: string;
  animalUnit: string;
  ownerName: string;
  ownerUnit: string;
  ownerRank: Rank;
  ownerObservations: string;
  pax: Pax;

  // Only FRONTEND
  isDuplicate: boolean;
  hasError: boolean;
}

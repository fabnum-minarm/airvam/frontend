export enum AutoPlacementModel {
  row = 'row',
  external = 'external',
  one_on_two = 'one_on_two',
}

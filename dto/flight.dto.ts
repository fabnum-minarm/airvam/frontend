import { Aircraft } from "~/dto/aircraft.dto";
import { User } from "~/dto/user.dto";
import { FlightDocument } from "~/dto/flight-document.dto";
import { Pax } from "~/dto/pax.dto";
import { Animal } from "~/dto/animal.dto";
import type { Leg } from "./leg.dto";

export class Flight {
  id: number;
  missionName: string;
  missionNumber: string;
  cotamNumber: string;
  aircraft: Aircraft;
  aircraftId: string | null;
  aircraftName: string | null;
  freeSeating: boolean = false;
  imperialSystem: boolean = false;
  regulatorUser: User;
  legUser: User;
  legs: Leg[] = [];
  documents: FlightDocument;
  archived: boolean;
  paxs: Pax[];
  animals: Animal[];
}

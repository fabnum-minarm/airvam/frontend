import { Airport } from "~/dto/airport.dto";
import { LegStatus } from "~/dto/legstatus.enum";
import { Pax } from "~/dto/pax.dto";
import { Animal } from "~/dto/animal.dto";
import { Seat } from "~/dto/seat.dto";
import { LegDocument } from "~/dto/leg-document.dto";

export class Leg {
  id: number;
  departure: Airport;
  arrival: Airport;
  departureTime: Date | null;
  arrivalTime: Date | null;
  status: keyof typeof LegStatus;
  paxs?: Pax[];
  animals?: Animal[];
  seats?: Seat[];
  documents?: LegDocument;
}

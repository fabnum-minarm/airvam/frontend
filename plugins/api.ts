import baseApi from '~/api/base-api'
import paxApi from '~/api/pax'
import animalApi from '~/api/animal'
import legApi from '~/api/leg'
import seatApi from '~/api/seat'
import flightApi from '~/api/flight'
import documentApi from '~/api/document'
import flightDocuments from "~/api/flight-documents";
import legDocuments from "~/api/leg-documents";
import userApi from '~/api/user';

export default defineNuxtPlugin(() => {
  const { token } = useAuthState()

  const runtimeConfig = useRuntimeConfig()

  const apiFetch = $fetch.create({
    baseURL: runtimeConfig.public.remote.baseURL,
    onRequest: (({ options }) => {
      if (token.value) {
        options.headers = {
          Authorization: token.value
        }
      }
    })
  })

  const api = {
    aircraft: baseApi(apiFetch, 'aircraft'),
    rank: baseApi(apiFetch, 'rank'),
    timezone: baseApi(apiFetch, 'timezone'),
    airport: baseApi(apiFetch, 'airport'),
    unit: baseApi(apiFetch, 'unit'),
    user: userApi(apiFetch, 'user'),
    userLight: baseApi(apiFetch, 'user/light'),
    flightPaginated: flightApi(apiFetch, 'flight'),
    pax: paxApi(apiFetch, 'pax'),
    animal: animalApi(apiFetch, 'animal'),
    leg: legApi(apiFetch, 'leg'),
    seat: seatApi(apiFetch, 'seat'),
    flightDocuments: flightDocuments(apiFetch, 'flight-document'),
    legDocuments: legDocuments(apiFetch, 'leg-document'),
    document: documentApi(apiFetch, 'document')
  };

  return {
    provide: { api, apiFetch }
  }
})

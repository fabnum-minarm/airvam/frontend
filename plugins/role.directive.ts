export default defineNuxtPlugin(({ vueApp }) => {
  const store = useStore();
  vueApp.directive('has-role', {
    mounted: function (el, binding) {
      if (!store.loggedInUser || !binding.value.includes(store.loggedInUser.role)) {
        el?.parentElement?.removeChild(el);
      }
    }
  })
})

import type { User } from "~/dto/user.dto";

export default defineNuxtPlugin(async () => {

  const store = useStore();

  const { $api, $config } = useNuxtApp();

  async function loadGlobal() {
    store.setAircrafts(await $api.aircraft.list());
    store.setRanks(await $api.rank.list());
    store.setTimezones(await $api.timezone.list());
    store.setAirports(await $api.airport.list());
    store.setUnits(await $api.unit.list());
    store.setUsers(await $api.user.list());
    store.setDocuments(await $api.document.list());
  }

  async function loadUsersDev() {
    store.setUsersLight(await $api.userLight.list());
  }

  if ($config.public.appEnv === 'dev') {
    await loadUsersDev();
  }

  const { status, data } = useAuthState()
  const { getSession } = useAuth()

  watch(status, async (state) => {
    if (state === 'authenticated') {
      store.setAuthenticated({
        user: data.value as User
      })
      await loadGlobal();
    }
  });

  const user = await getSession();
  store.setAuthenticated({
    user: user as unknown as User
  })
  if (user) {
    loadGlobal()
  }
})

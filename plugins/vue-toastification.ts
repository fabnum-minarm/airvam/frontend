import install, { useToast, TYPE } from 'vue-toastification'
import 'vue-toastification/dist/index.css'

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.use(install, {
        hideProgressBar: true,
        closeButton: false,
        toastDefaults: {
            [TYPE.SUCCESS]: {
                icon: {
                    iconClass: 'material-icons',
                    iconChildren: 'check_circle'
                }
            },
            [TYPE.INFO]: {
                icon: {
                    iconClass: 'material-icons',
                    iconChildren: 'info'
                }
            },
            [TYPE.ERROR]: {
                icon: {
                    iconClass: 'material-icons',
                    iconChildren: 'error_outline'
                }
            },
            [TYPE.WARNING]: {
                icon: {
                    iconClass: 'material-icons',
                    iconChildren: 'warning_amber'
                }
            }
        }
    })


    return {
        provide: {
            toast: useToast()
        }
    }
})

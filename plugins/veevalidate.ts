import { defineRule, Field } from 'vee-validate';
import { required, min, max, between, integer, alpha, email } from "@vee-validate/rules";

export default defineNuxtPlugin((nuxtApp) => {

  nuxtApp.vueApp.component('ValidationProvider', Field);
  nuxtApp.vueApp.component('Field', Field);

  defineRule("required", value =>
    required(value) || 'Le champ est obligatoire.'
  );

  defineRule("min", (value, [length]) =>
    min(value, [length]) || `Le champ doit faire ${length} caractères minimum.`
  );

  defineRule("min_date", (value, [min, dateString]) =>
    (new Date(min) < new Date(value)) || `Le champ doit être supérieur à ${dateString}.`
  );

  defineRule("max", (value, [length]) =>
    max(value, [length]) || `Le champ doit faire ${length} caractères maximum.`
  );

  defineRule("alpha", value =>
    alpha(value, ['']) || 'Le champ ne doit contenir que des lettres.'
  );

  defineRule("uppercase", value =>
    value.match(/^[A-Z]*$/) || 'Le champ ne doit contenir que des lettres majuscules.'
  );

  defineRule("between", (value, [min, max]) =>
    between(value, [min, max]) || `Le nombre doit être compris entre ${min} et ${max}.`);

  defineRule("integer", value =>
    integer(value) || 'Le champ doit être un nombre.'
  );

  defineRule("email", value =>
    email(value) || 'Le champ doit être un email valide.'
  );

  defineRule("decimal", value =>
    decimalValidate(value).valid || 'Le champ doit être un nombre, utilisez le point . pour séparer les décimales (ex: 12.3).'
  );

  defineRule('matchBalancing', (_value: unknown, [currentValue, total]) =>
    currentValue === total || `La somme cumulée des zones doit faire ${total}.`
  );
})

export const decimalValidate = (value: any, { decimals = '*', separator = '.' } = {}) => {
  if (value === null || value === undefined || value === '') {
    return {
      valid: false
    };
  }
  if (Number(decimals) === 0) {
    return {
      valid: /^-?\d*$/.test(value),
    };
  }
  const regexPart = decimals === '*' ? '+' : `{1,${decimals}}`;
  const regex = new RegExp(`^[-+]?\\d*(\\${separator}\\d${regexPart})?([eE]{1}[-]?\\d+)?$`);

  return {
    valid: regex.test(value),
  };
}

<p align="center">
  <a href="https://beta.gouv.fr/startups/airvam.html" target="blank">
    <img src="https://airvam.igloo.fabnum.fr/airvam.svg" width="100" alt="Logo Air VAM" />
  </a>
</p>

# Air VAM
Gérer simplement les dossiers liés à un vol militaire aérien

## Architecture

Frontend sous [Nuxt 2.x](https://nuxtjs.org/fr/) couplé à [Vuetify](https://vuetifyjs.com/en/).

## Installation

```bash
# Installation des dépendances
$ yarn install

# Serveur de développement avec hot reload sur localhost:4200
$ yarn dev

# Build pour production et lancement du serveur
$ yarn build
$ yarn start
```

Pour plus d'informations sur l'utilisation de Nuxt: [documentation](https://nuxtjs.org).

## Tests e2e

Les tests E2E sont gérés avec [Cypress](https://www.cypress.io/).

### Prérequis

```bash
# Serveur de développement avec hot reload sur localhost:4200
$ yarn dev
```

### Lancement des tests

```bash
# Démarrage de l'interface graphique de Cypress pour lancer les tests un à un et debug
$ yarn cy:open
```

```bash
# Lancement de tout les tests e2e
$ yarn cy:run
```

# License

[MIT licensed](LICENSE).

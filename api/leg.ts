import type { $Fetch } from "nitropack";
import baseApi from '~/api/base-api'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const legApi: any = baseApi(apiFetch, resourcePath);

  legApi.autoPlacement = (payload: any, legId: string) => {
    return apiFetch(`/${resourcePath}/${legId}/autoPlacement`, { method: 'post', body: payload })
  }

  legApi.getStatus = (legId: number) => {
    return apiFetch(`/${resourcePath}/${legId}/status`)
  }

  legApi.getDocuments = (legId: number) => {
    return apiFetch(`/${resourcePath}/${legId}/documents`)
  }

  legApi.finalize = (legId: number, payload: any) => {
    return apiFetch(`/${resourcePath}/${legId}/finalize`, { method: 'post', body: payload })
  }

  return legApi;
};

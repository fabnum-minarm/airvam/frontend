import baseApiPagination from "~/api/base-api-pagination";
import type { $Fetch } from 'nitropack';

export default (apiFetch: $Fetch, resourcePath: string) => {
  const flightApi: any = baseApiPagination(apiFetch, resourcePath);

  flightApi.getDocuments = (flightId: string) => {
    return apiFetch(`/${resourcePath}/${flightId}/documents`)
  }

  return flightApi;
};

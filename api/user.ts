import baseApi from '~/api/base-api'
import type { $Fetch } from 'nitropack'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const userApi: any = baseApi(apiFetch, resourcePath);

  userApi.deletePermanent = (userEmail: string) => {
    return apiFetch(`/${resourcePath}/${userEmail}/permanent`, { method: 'delete' })
  }

  return userApi;
};

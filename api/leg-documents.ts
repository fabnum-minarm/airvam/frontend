import type { $Fetch } from "nitropack";
import baseApi from '~/api/base-api'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const legDocumentsApi: any = baseApi(apiFetch, resourcePath);

  legDocumentsApi.getDocuments = (flightId: string, legId: string) => {
    return apiFetch.raw(`/${resourcePath}/${flightId}/${legId}`, { responseType: 'blob' }) 
  }

  legDocumentsApi.getOneDocument = (flightId: string, legId: string, documentId: string) => {
    return apiFetch.raw(`/${resourcePath}/${flightId}/${legId}/${documentId}`, { responseType: 'arrayBuffer' })
  }

  return legDocumentsApi;
};

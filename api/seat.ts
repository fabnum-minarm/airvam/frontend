import type { $Fetch } from "nitropack";
import baseApi from '~/api/base-api'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const seatApi: any = baseApi(apiFetch, resourcePath);

  seatApi.comment = (payload: any, seatId: string) => {
    return apiFetch(`/${resourcePath}/${seatId}/comment`, { method: 'post', body: { observations: payload } })
  }

  seatApi.lock = (seatId: string) => {
    return apiFetch(`/${resourcePath}/${seatId}/lock`, { method: 'post' })
  }

  seatApi.unlock = (seatId: string) => {
    return apiFetch(`/${resourcePath}/${seatId}/unlock`, { method: 'post' })
  }

  seatApi.place = (seatId: string, paxId: string) => {
    return apiFetch(`/${resourcePath}/${seatId}/place/${paxId}`, { method: 'post' })
  }

  return seatApi;
};

import type { $Fetch } from "nitropack";
import baseApi from '~/api/base-api'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const paxApi: any = baseApi(apiFetch, resourcePath);

  paxApi.parse = (flightId: string, file: any) => {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    const options = {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': 'application/json'
      },
      reportProgress: true,
    };

    return apiFetch(`/${resourcePath}/parse/${flightId}`, { method: 'post', body: formData, ...options })
  }

  paxApi.updateList = (payload: any, flightId: string) => {
    return apiFetch(`/${resourcePath}/updateList/${flightId}`, { method: 'post', body: payload })
  }

  paxApi.exportList = (flightId: string) => {
    return apiFetch(`/${resourcePath}/export/${flightId}`, { responseType: 'blob' })
  }

  return paxApi;
};

import type { $Fetch } from 'nitropack';
export default (apiFetch: $Fetch, resource: string) => ({
  list(query: string, pageToGet: number = 1, filters: any = {}) {
    const params = {
      page: `${pageToGet}`,
      limit: `10`,
      query: query ? query : ''
    };

    return apiFetch(`/${resource}/search`, { body: filters, params, method: 'post' })
  },

  create(payload: any) {
    return apiFetch(`/${resource}`, { body: payload, method: 'post' })
  },

  get(id: string) {
    return apiFetch(`/${resource}/${id}`)
  },

  update(payload: any, id: string) {
    return apiFetch(`/${resource}/${id}`, { body: payload, method: 'patch' })
  },

  delete(id: string) {
    return apiFetch(`/${resource}/${id}`, { method: 'delete' })
  },

  activate(id: string) {
    return apiFetch(`/${resource}/${id}/activate`, { method: 'patch' })
  }

})

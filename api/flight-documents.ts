import type { $Fetch } from "nitropack";
import baseApi from '~/api/base-api'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const flightDocumentsApi: any = baseApi(apiFetch, resourcePath);

  flightDocumentsApi.getDocuments = (flightId: string) => {
    return apiFetch.raw(`/${resourcePath}/${flightId}`, { responseType: 'blob' })
  }

  flightDocumentsApi.getOneDocument = (flightId: string, documentId: string) => {
    return apiFetch.raw(`/${resourcePath}/${flightId}/${documentId}`, { responseType: 'arrayBuffer' })
  }

  return flightDocumentsApi;
};

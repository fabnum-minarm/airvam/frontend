import type { $Fetch } from "nitropack";
import baseApi from '~/api/base-api'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const documentApi: any = baseApi(apiFetch, resourcePath);

  documentApi.create = (document: any) => {
    const formData: FormData = new FormData();
    formData.append('file', document.name, document.name.name);
    if(document.category) {
      formData.append('category', document.category);
    }

    const options = {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': 'application/json'
      },
      reportProgress: true,
    };

    return apiFetch(`/${resourcePath}`, { method: 'post', body: formData, ...options })
  }

  documentApi.getOneDocument = (docId: string) => {
    return apiFetch(`/${resourcePath}/${docId}`, { responseType: 'arrayBuffer' })
  }

  return documentApi;
};

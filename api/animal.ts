import type { $Fetch } from "nitropack";
import baseApi from '~/api/base-api'

export default (apiFetch: $Fetch, resourcePath: string) => {
  const animalApi: any = baseApi(apiFetch, resourcePath);

  animalApi.parse = (flightId: string, file: any) => {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    const options = {
      headers: {
        // 'Content-Type': 'multipart/form-data', // TODO: check if it's right, with error: Boundary not found
        'Accept': 'application/json'
      },
      reportProgress: true,
    };

    return apiFetch(`/${resourcePath}/parse/${flightId}`, { method: 'post', body: formData, ...options })
  }

  animalApi.updateList = (payload: any, flightId: string) => {
    return apiFetch(`/${resourcePath}/updateList/${flightId}`, { method: 'post', body: payload })
  }

  animalApi.exportList = (flightId: string) => {
    return apiFetch(`/${resourcePath}/export/${flightId}`, { responseType: 'blob' })
  }

  return animalApi;
};

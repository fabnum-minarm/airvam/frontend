import { Aircraft } from "~/dto/aircraft.dto";
import { Rank } from "~/dto/rank.dto";
import { Timezone } from "~/dto/timezone.dto";
import { Airport } from "~/dto/airport.dto";
import { User } from "~/dto/user.dto";
import { Flight } from "~/dto/flight.dto";
import { Unit } from "~/dto/unit.dto";
import { PaginatedResult } from "~/dto/paginated-result.dto";
import { Document } from "~/dto/document.dto";
import { defineStore } from 'pinia';

type State = {
    isMobile: boolean
    aircrafts: Aircraft[]
    ranks: Rank[]
    timezones: Timezone[]
    airports: Airport[]
    units: Unit[]
    users: User[]
    usersLight: User[]
    flightsPaginated: PaginatedResult<Flight>
    documents: Document[]

    auth?: {
        loggedIn: boolean
        user: User
    }
}

export const useStore = defineStore('store', {
    state: (): State => ({
        isMobile: false,
        aircrafts: [],
        ranks: [],
        timezones: [],
        airports: [],
        units: [],
        users: [],
        usersLight: [],
        flightsPaginated: new PaginatedResult<Flight>(),
        documents: []
    }),

    getters: {
        ranksLight: (s) => s.ranks.filter((r: any) => !r.disabled),
        airportsEnabled: (s) => s.airports.filter((a: any) => !a.disabled),
        unitsEnabled: (s) => s.units.filter((a: any) => !a.disabled),
        usersLightEnabled: (s) => s.usersLight.filter((s: any) => !s.disabled),
        fullUsers: (s) => s.users,

        isAuthenticated(state): boolean {
            return state.auth?.loggedIn ?? false
        },

        loggedInUser(state) {
            return state.auth?.user
        }
    },
    actions: {
        setIsMobile(isMobile = false) {
            this.isMobile = isMobile;
        },

        setAircrafts(aircrafts: Aircraft[]) {
            this.aircrafts = aircrafts;
        },

        setRanks(ranks: Rank[]) {
            this.ranks = ranks;
        },

        setTimezones(timezones: Timezone[]) {
            this.timezones = timezones;
        },

        setAirports(airports: Airport[]) {
            this.airports = airports;
        },

        setUnits(units: Unit[]) {
            this.units = units;
        },

        setUsers(users: User[]) {
            this.users = users;
        },

        setUsersLight(users: User[]) {
            this.usersLight = users;
        },

        setFlightsPaginated(flights: PaginatedResult<Flight>) {
            flights.items.map(f => f.legs.map(l => {
                l.departureTime = l.departureTime ? new Date(l.departureTime) : null;
                l.arrivalTime = l.arrivalTime ? new Date(l.arrivalTime) : null;
                return l;
            }));
            this.flightsPaginated = flights;
            this.flightsPaginated.meta.last = flights.meta.currentPage >= flights.meta.totalPages;
        },

        setDocuments(documents: Document[]) {
            this.documents = documents;
        },

        editList(payload: { listName: keyof State, item: any, id?: string }) {
            const id = payload.id ? payload.id : 'id';
            const list = this[payload.listName] as Array<any>;
            const itemIndex = list.findIndex(i => i[id] === payload.item[id]);
            if (itemIndex < 0) {
                list.unshift(payload.item);
            } else {
                list[itemIndex] = payload.item;
            }
        },

        deleteFromList(payload: { listName: keyof State, item: any, id?: string }) {
            const id = payload.id ? payload.id : 'id';
            const list = this[payload.listName] as Array<any>;
            const itemIndex = list.findIndex(i => i[id] === payload.item[id]);
            if (itemIndex >= 0) {
                list.splice(itemIndex, 1);
            }
        },

        setAuthenticated(payload: { user: User }) {
            this.auth = payload.user ? {
                loggedIn: true,
                user: payload.user
            } : undefined
        }
    }
});

<template>
  <v-app :style="{ background: themeBackground }">
    <v-row class="login-wrapper">
      <v-col cols="12" sm="6">
        <v-img alt="Logo Airvam" contain width="50%" src="/airvam.svg">
        </v-img>
      </v-col>
      <v-col cols="12" sm="6">
        <v-spacer></v-spacer>
        <h1 class="mb-6">Connexion</h1>
        <div v-if="$config.public.appEnv === 'dev'">
          <h2>Only for DEV</h2>
          <v-select label="Utilisateur" data-cy="LoginDevUserList" v-model="user" :items="users"
            :item-title="(user: User) => user.email + ' - ' + user.role" variant="outlined" return-object>
          </v-select>
          <v-btn variant="outlined" class="inverted" @click="localLogin()" :disabled="!user" data-cy="LoginDevBtn" rounded size="large">
            Se connecter
          </v-btn>
        </div>
        <div v-if="$config.public.appEnv === 'test'">
          <h2>Only for TEST</h2>
          <v-text-field label="Email" v-model="user" variant="outlined">
          </v-text-field>
          <v-btn variant="outlined" class="inverted" @click="localLogin()" :disabled="!user" data-cy="LoginDevBtn" rounded size="large">
            Se connecter
          </v-btn>
        </div>
        <v-btn variant="outlined" @click="login()" rounded size="large">
          <v-img width="30px" contain alt="Logo MindefConnect" src="/logo_mindef.png">
          </v-img>
          &nbsp;MindefConnect
        </v-btn>
        <v-spacer></v-spacer>
        <div class="d-flex align-center justify-space-between">
          <v-img width="110px" contain alt="Logo Ministère des Armées" src="/logo_ministere_armees.svg">
          </v-img>
          <a target="_blank" rel="noopener" href="https://beta.gouv.fr/startups/?incubateur=fabnumdef">
            <v-img width="150" contain alt="Logo Fabrique Numérique" src="/fabnum.svg">
            </v-img>
          </a>
          <div v-if="$config.public.appEnv === 'dev'">
            Version {{ appVersion }}
          </div>
        </div>
      </v-col>
    </v-row>
  </v-app>
</template>

<script setup lang="ts">
import { version } from '../../package.json';
import { User } from "~/dto/user.dto";

useHead({
  meta: [
    {
      hid: 'robots',
      name: 'robots',
      content: 'noindex, nofollow'
    }
  ]
})

definePageMeta({
  auth: {
    unauthenticatedOnly: true,
    navigateAuthenticatedTo: '/',
  }
})

const { $vuetify, $apiFetch, $toast, $config } = useNuxtApp();
const store = useStore();
const user = ref<User | null>(null);
const appVersion = ref(version);

const { signIn } = useAuth();

// TODO: put this logic into a composable
const themeBackground = computed(() => {
  const theme = $vuetify.theme.current.value.dark ? 'dark' : 'light';
  return $vuetify.theme.themes.value?.[theme].colors.background;
})

const users: Ref<User[]> = computed(() => {
  return store?.usersLightEnabled;
});

const login = async () => {
  try {
    window.location.href = await $apiFetch('/auth/login'); // @ts-ignore 
  } catch (e: any) {
    $toast.error("Erreur lors de la récupération du lien de connexion MINDEF Connect");
    console.error(e.response?.data?.message);
  }
}

const localLogin = async () => {
  try {
    await signIn({
      // TODO: should be user.value instead of user.value?.email when not in dev ?
      email: $config.public.appEnv === 'dev' ? user.value?.email : user.value?.email,
    }, { external: true });
  } catch (e: any) {
    const message = e.response?.data?.message ? e.response?.data?.message : 'Erreur lors de la connexion';
    $toast.error(message);
    console.error(e.response?.data?.message);
  }
}

</script>

<style lang="scss">
@import "../../assets/variables.scss";

.login-wrapper {
  height: 100vh;
  margin: 0;

  .v-col-12 {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;

    &:first-child {
      background-color: $color-primary;
    }

    &:last-child {
      align-items: start;
      padding-left: 5%;
    }
  }
}
</style>

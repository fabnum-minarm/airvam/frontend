import axios from "axios";

Cypress.Commands.add('devLogin', (userEmail) => {
  cy.logout();
  cy.get('[data-cy=LoginDevUserList]').parent().click();
  cy.get(".v-overlay-container .v-list .v-list-item").contains(userEmail).click();
  cy.get('[data-cy=LoginDevBtn]').click();
  cy.url().should('match', /vols/);
});

Cypress.Commands.add('logout', () => {
  cy.clearAllLocalStorage();
  cy.clearAllCookies();
  cy.visit('/');
  cy.url().should('match', /login/);
});

Cypress.Commands.add('logoutUI', () => {
  cy.get('[data-cy=LogoutUser]').click({force: true});
  cy.get(".v-overlay-container .v-list .v-list-item").contains('Déconnexion').click({ force: true });
});

Cypress.Commands.add('populateDb', async (endpoint) => {
  await axios.post(`${Cypress.env('apiUrl')}/${endpoint}/populateTestData`);
});

Cypress.Commands.add('resetDb', async (endpoint) => {
  await axios.post(`${Cypress.env('apiUrl')}/${endpoint}/clearTestData`);
});

describe('Administration des utilisateurs', () => {
  beforeEach(() => {
    cy.logout();
  });

  after(() => {
    cy.resetDb('user');
  });

  it(`Un utilisateur ne doit pas pouvoir accéder à la gestion des utilisateurs`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/administration');
    cy.url().should('match', /vols/)
  });

  const userFirstname = `CYTEST${Math.floor(Math.random() * 10000)}`;
  const userLastname = `CYTEST${Math.floor(Math.random() * 10000)}`;
  const userEmail = `CYTEST${Math.floor(Math.random() * 10000)}@example.com`;
  const userAdminEmail = `CYTEST${Math.floor(Math.random() * 10000)}@example.com`;
  const userGodEmail = `CYTEST${Math.floor(Math.random() * 10000)}@example.com`;
  const userRole = 'Super Administrateur';
  const userAdminRole = 'Administrateur';
  const userRank = 'OF-10 - OTAN';
  const userUnit = 'ISTRES';
  const userAdminUnit = 'MINARM';
  const stringOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
  const stringOver50 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
  describe('Administration des utilisateurs - Super admin', () => {
    beforeEach(() => {
      cy.devLogin(Cypress.env('devTestGod'));
      cy.visit('/administration');
      cy.get('main .v-tabs').contains('Utilisateurs').click();
    });

    it(`Un super administrateur doit pouvoir accéder à la gestion des utilisateurs`, () => {
      cy.get('main .v-tabs').contains('Utilisateurs').should('exist');
      cy.get('[data-cy=AddUserBtn]').should('exist');
      cy.get('main .airvam-table').contains('Rôle').should('exist');
    });


    it(`Un super administrateur doit pouvoir ajouter un utilisateur`, () => {
      cy.get('[data-cy=AddUserBtn]').click();
      cy.get('.v-dialog').contains(`Création d'un utilisateur`).should('exist');
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      // Si le formulaire n'est pas valide, même au click forcé sur le bouton il ne doit rien se passer
      cy.get('[data-cy=UserFormOkBtn]').invoke('removeAttr', 'disabled');
      cy.get('[data-cy=UserFormOkBtn]').invoke('removeClass', 'v-btn--disabled');
      cy.get('[data-cy=UserFormOkBtn]').click();
      cy.wait(500);
      cy.get('.v-dialog').contains(`Création d'un utilisateur`).should('exist');
      cy.get('[data-cy=UserFormCancelBtn]').click();
      cy.get('[data-cy=AddUserBtn]').click();

      cy.get('[data-cy=UserFormEmailInput] input').type(userEmail);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UserFormFirstnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UserFormLastnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UserFormRoleInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userRole).click();
      cy.get('[data-cy=UserFormOkBtn]').should('not.be.disabled');

      // Test de la validité du formulaire
      // Email
      cy.get('[data-cy=UserFormEmailInput] input').clear();
      cy.get('[data-cy=UserFormEmailInput] input').type(`${stringOver255}@example.com`);
      cy.get('[data-cy=UserFormEmailInput] input').should('have.value', `${stringOver255}@example.com`);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormEmailInput] input').clear();
      cy.get('[data-cy=UserFormEmailInput] input').type(`NOT A MAIL`);
      cy.get('[data-cy=UserFormEmailInput] input').should('have.value', `NOT A MAIL`);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormEmailInput] input').clear();
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormEmailInput] input').type(userEmail);
      cy.get('[data-cy=UserFormOkBtn]').should('not.be.disabled');

      // First Name
      cy.get('[data-cy=UserFormFirstnameInput] input').clear();
      cy.get('[data-cy=UserFormFirstnameInput] input').type(stringOver50);
      cy.get('[data-cy=UserFormFirstnameInput] input').should('have.value', stringOver50);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormFirstnameInput] input').clear();
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormFirstnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormOkBtn]').should('not.be.disabled');

      // Last Name
      cy.get('[data-cy=UserFormLastnameInput] input').clear();
      cy.get('[data-cy=UserFormLastnameInput] input').type(stringOver50);
      cy.get('[data-cy=UserFormLastnameInput] input').should('have.value', stringOver50);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormLastnameInput] input').clear();
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormLastnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormOkBtn]').should('not.be.disabled');

      // Annuler
      cy.get('[data-cy=UserFormCancelBtn]').click();
      cy.get('main .airvam-table tr:not(.disabled)').contains(userEmail).should('not.exist');

      // Ajouter
      cy.intercept({
        method: 'POST',
        url: '/api/user',
      }).as('createUser');

      cy.get('[data-cy=AddUserBtn]').click();
      cy.get('[data-cy=UserFormEmailInput] input').type(userEmail);
      cy.get('[data-cy=UserFormFirstnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormLastnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormRoleInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userRole).click();

      cy.get('[data-cy=UserFormRankInput] input').type(userRank, {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userRank).click();

      cy.get('[data-cy=UserFormUnitInput] input').type(userUnit, {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userUnit).click();

      cy.get('[data-cy=UserFormOkBtn]').click();
      cy.wait('@createUser').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(userEmail).should('exist');
    });


    it(`Un super administrateur doit pouvoir éditer un utilisateur`, () => {
      cy.get('main .airvam-table tr:not(.disabled)').contains(userEmail).parents('tr').find('[data-cy=EditUserBtn]').click();
      cy.get('.v-dialog').contains(`Édition d'un utilisateur`).should('exist');
      cy.get('[data-cy=UserFormEmailInput] input').should('have.value', userEmail);
      cy.get('[data-cy=UserFormFirstnameInput] input').should('have.value', userFirstname);
      cy.get('[data-cy=UserFormLastnameInput] input').should('have.value', userLastname);
      cy.get('[data-cy=UserFormRankInput] input').should('have.value', userRank);
      cy.get('[data-cy=UserFormUnitInput] input').should('have.value', userUnit);
      cy.get('[data-cy=UserFormRoleInput] input').should('exist');
      cy.get('[data-cy=UserFormRoleInput] input').parents('.v-input').find('.v-select__selection').should('have.text', userRole);
      cy.get('[data-cy=UserFormOkBtn]').should('not.be.disabled');

      cy.intercept({
        method: 'PATCH',
        url: '/api/user/**',
      }).as('editUser');
      cy.get('[data-cy=UserFormEmailInput] input').should('be.disabled');
      cy.get('[data-cy=UserFormFirstnameInput] input').clear();
      cy.get('[data-cy=UserFormFirstnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormLastnameInput] input').clear();
      cy.get('[data-cy=UserFormLastnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormOkBtn]').click();
      cy.wait('@editUser').its('response.statusCode').should('equal', 200);
      cy.get('main .airvam-table tr:not(.disabled)').contains(userEmail).should('exist');
    });

    it(`Un super administrateur doit pouvoir activer / désactiver un utilisateur`, () => {
      cy.get('[data-cy=UserListSearchInput] input').type(userEmail);
      cy.get('main .airvam-table tr:not(.disabled)').contains(userEmail).parents('tr').find('[data-cy=DeleteUserBtn]').click();
      cy.get('.v-dialog').contains(`Désactivation d'un utilisateur`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'DELETE',
        url: '/api/user/**',
      }).as('deleteUser');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deleteUser').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=UserListSearchInput] input').clear();
      cy.get('[data-cy=UserListSearchInput] input').type(userEmail);
      cy.get('main .airvam-table tr.disabled').contains(userEmail).should('exist');

      cy.get('main .airvam-table tr.disabled').contains(userEmail).parents('tr').find('[data-cy=DeleteUserBtn]').click();
      cy.get('.v-dialog').contains(`Activation d'un utilisateur`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'PATCH',
        url: '/api/user/**/activate',
      }).as('activateUser');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@activateUser').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=UserListSearchInput] input').clear();
      cy.get('[data-cy=UserListSearchInput] input').type(userEmail);
      cy.get('main .airvam-table tr:not(.disabled)').contains(userEmail).should('exist');
    });

    it(`Un super administrateur doit pouvoir supprimer définitivement un utilisateur`, () => {
      cy.get('[data-cy=UserListSearchInput] input').type(userEmail);
      cy.get('main .airvam-table tr').contains(userEmail).parents('tr').find('[data-cy=DeletePermanentUserBtn]').should('not.exist');
      cy.get('main .airvam-table tr:not(.disabled)').contains(userEmail).parents('tr').find('[data-cy=DeleteUserBtn]').click();
      cy.get('.v-dialog').contains(`Désactivation d'un utilisateur`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');
      cy.intercept({
        method: 'DELETE',
        url: '/api/user/**',
      }).as('deleteUser');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deleteUser').its('response.statusCode').should('equal', 200);
      cy.get('main .airvam-table tr').contains(userEmail).parents('tr').find('[data-cy=DeletePermanentUserBtn]').click();
      cy.get('.v-dialog').contains(`Suppression d'un utilisateur`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'DELETE',
        url: '/api/user/**/permanent',
      }).as('deletePermanentUser');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deletePermanentUser').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=UserListSearchInput] input').clear();
      cy.get('[data-cy=UserListSearchInput] input').type(userEmail);
      cy.get('main .airvam-table tr').contains(userEmail).should('not.exist');
    });
  });

  describe('Administration des unités - Admin', () => {
    // Création d'un user super admin sur le scope de l'admin
    before(() => {
      cy.devLogin(Cypress.env('devTestGod'));
      cy.visit('/administration');
      cy.get('main .v-tabs').contains('Utilisateurs').click();
      cy.get('[data-cy=AddUserBtn]').click();

      cy.get('[data-cy=UserFormEmailInput] input').type(userGodEmail);
      cy.get('[data-cy=UserFormFirstnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormLastnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormRoleInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userRole).click();
      cy.get('[data-cy=UserFormUnitInput] input').type(userAdminUnit, {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userAdminUnit).click();

      cy.intercept({
        method: 'POST',
        url: '/api/user',
      }).as('createUser');

      cy.get('[data-cy=UserFormOkBtn]').click();
      cy.wait('@createUser').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(userGodEmail).should('exist');

      cy.logoutUI();
    });

    beforeEach(() => {
      cy.devLogin(Cypress.env('devTestAdmin'));
      cy.visit('/administration');
      cy.get('main .v-tabs').contains('Utilisateurs').click();
    });

    it(`Un administrateur doit pouvoir accéder à la gestion des utilisateurs de son scope`, () => {
      cy.get('main .v-tabs').contains('Utilisateurs').should('exist');
      cy.get('[data-cy=AddUserBtn]').should('exist');
      cy.get('main .airvam-table').contains('Rôle').should('exist');
    });

    it(`Un administrateur doit pouvoir ajouter un utilisateur sur son scope`, () => {
      cy.get('[data-cy=AddUserBtn]').click();
      cy.get('.v-dialog').contains(`Création d'un utilisateur`).should('exist');
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UserFormEmailInput] input').type(userAdminEmail);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UserFormFirstnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UserFormLastnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UserFormRoleInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userAdminRole).click();
      cy.get('[data-cy=UserFormOkBtn]').should('not.be.disabled');

      // Check des rôles
      cy.get('[data-cy=UserFormRoleInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userRole).should('not.exist');

      // Check des unités
      cy.get('[data-cy=UserFormUnitInput] input').type(userUnit, {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userUnit).should('not.exist');

      // Annuler
      cy.get('[data-cy=UserFormCancelBtn]').click();
      cy.get('main .airvam-table tr:not(.disabled)').contains(userAdminEmail).should('not.exist');

      // Ajouter
      cy.intercept({
        method: 'POST',
        url: '/api/user',
      }).as('createUser');

      cy.get('[data-cy=AddUserBtn]').click();
      cy.get('[data-cy=UserFormEmailInput] input').type(userAdminEmail);
      cy.get('[data-cy=UserFormFirstnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormLastnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormRoleInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userAdminRole).click();

      cy.get('[data-cy=UserFormRankInput] input').type(userRank, {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userRank).click();

      cy.get('[data-cy=UserFormUnitInput] input').type(userAdminUnit, {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(userAdminUnit).click();

      cy.get('[data-cy=UserFormOkBtn]').click();
      cy.wait('@createUser').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(userAdminEmail).should('exist');
    });

    it(`Un administrateur doit pouvoir éditer un utilisateur de son scope`, () => {
      cy.get('main .airvam-table tr').contains(userEmail).should('not.exist');

      cy.get('main .airvam-table tr:not(.disabled)').contains(userAdminEmail).parents('tr').find('[data-cy=EditUserBtn]').click();
      cy.get('.v-dialog').contains(`Édition d'un utilisateur`).should('exist');
      cy.get('[data-cy=UserFormEmailInput] input').should('have.value', userAdminEmail);
      cy.get('[data-cy=UserFormFirstnameInput] input').should('have.value', userFirstname);
      cy.get('[data-cy=UserFormLastnameInput] input').should('have.value', userLastname);
      cy.get('[data-cy=UserFormRankInput] input').should('have.value', userRank);
      cy.get('[data-cy=UserFormUnitInput] input').should('have.value', userAdminUnit);
      cy.get('[data-cy=UserFormRoleInput] input').should('exist');
      cy.get('[data-cy=UserFormRoleInput] input').parents('.v-input').find('.v-select__selection').should('have.text', userAdminRole);
      cy.get('[data-cy=UserFormOkBtn]').should('not.be.disabled');

      cy.intercept({
        method: 'PATCH',
        url: '/api/user/**',
      }).as('editUser');
      cy.get('[data-cy=UserFormEmailInput] input').should('be.disabled');
      cy.get('[data-cy=UserFormFirstnameInput] input').clear();
      cy.get('[data-cy=UserFormFirstnameInput] input').type(userLastname);
      cy.get('[data-cy=UserFormLastnameInput] input').clear();
      cy.get('[data-cy=UserFormLastnameInput] input').type(userFirstname);
      cy.get('[data-cy=UserFormOkBtn]').click();
      cy.wait('@editUser').its('response.statusCode').should('equal', 200);
      cy.get('main .airvam-table tr:not(.disabled)').contains(userAdminEmail).should('exist');

      cy.get('main .airvam-table tr:not(.disabled)').contains(userGodEmail).parents('tr').find('[data-cy=EditUserBtn]').should('be.disabled');
    });

    it(`Un administrateur doit pouvoir désactiver un utilisateur de son scope`, () => {
      cy.get('main .airvam-table tr:not(.disabled)').contains(userAdminEmail).parents('tr').find('[data-cy=DeleteUserBtn]').click();
      cy.get('.v-dialog').contains(`Désactivation d'un utilisateur`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'DELETE',
        url: '/api/user/**',
      }).as('deleteUser');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deleteUser').its('response.statusCode').should('equal', 200);
      cy.get('main .airvam-table tr.disabled').contains(userAdminEmail).should('exist');

      cy.get('main .airvam-table tr:not(.disabled)').contains(userGodEmail).parents('tr').find('[data-cy=DeleteUserBtn]').should('be.disabled');
    });
  })

});

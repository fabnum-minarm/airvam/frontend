describe('Administration des codes ICAO', () => {
  after(() => {
    cy.resetDb('airport');
  });

  it(`Un utilisateur ne doit pas pouvoir accéder à la gestion des codes ICAO`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/administration');
    cy.url().should('match', /vols/);
  });

  it(`Un administrateur ne doit pas pouvoir accéder à la gestion des codes ICAO`, () => {
    cy.devLogin(Cypress.env('devTestAdmin'));
    cy.visit('/administration');
    cy.get('main .v-tabs').contains('Codes ICAO').should('not.exist');
  });

  describe('Administration des codes ICAO - Super admin', () => {
    beforeEach(() => {
      cy.devLogin(Cypress.env('devTestGod'));
      cy.visit('/administration');
      cy.get('main .v-tabs').contains('Codes ICAO').click();
    });

    it(`Un super administrateur doit pouvoir accéder à la gestion des codes ICAO`, () => {
      cy.get('main .v-tabs').contains('Codes ICAO').should('exist');
      cy.get('[data-cy=AddAirportBtn]').should('exist');
      cy.get('main .airvam-table').contains('OACI').should('exist');
    });

    const airportName = `CYTEST${Math.floor(Math.random() * 1000)}`;
    const airportOACI = `ABCD`;
    const airportIATA = `ABC`;
    const airportTimezone = 'Europe/Paris';
    it(`Un super administrateur doit pouvoir ajouter un code ICAO`, () => {
      cy.get('[data-cy=AddAirportBtn]').click();
      cy.get('.v-dialog').contains(`Création d'un aéroport`).should('exist');
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      // Si le formulaire n'est pas valide, même au click forcé sur le bouton il ne doit rien se passer
      cy.get('[data-cy=AirportFormOkBtn]').invoke('removeAttr', 'disabled');
      cy.get('[data-cy=AirportFormOkBtn]').invoke('removeClass', 'v-btn--disabled');
      cy.get('[data-cy=AirportFormOkBtn]').click();
      cy.wait(500);
      cy.get('.v-dialog').contains(`Création d'un aéroport`).should('exist');
      cy.get('[data-cy=AirportFormCancelBtn]').click();
      cy.get('[data-cy=AddAirportBtn]').click();

      cy.get('[data-cy=AirportFormOaciInput] input').type(airportOACI);
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=AirportFormIataInput] input').type(airportIATA);
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=AirportFormTimezoneInput] input').type(airportTimezone);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(airportTimezone).click();
      cy.get('[data-cy=AirportFormOkBtn]').should('not.be.disabled');

      // Test de la validité du formulaire
      // OACI
      cy.get('[data-cy=AirportFormOaciInput] input').clear();
      cy.get('[data-cy=AirportFormOaciInput] input').type('a');
      cy.get('[data-cy=AirportFormOaciInput] input').should('have.value', 'a');
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormOaciInput] input').clear();
      cy.get('[data-cy=AirportFormOaciInput] input').type('abcd');
      cy.get('[data-cy=AirportFormOaciInput] input').should('have.value', 'abcd');
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormOaciInput] input').clear();
      cy.get('[data-cy=AirportFormOaciInput] input').type('ABCDE');
      cy.get('[data-cy=AirportFormOaciInput] input').should('have.value', 'ABCDE');
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormOaciInput] input').clear();
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormOaciInput] input').type(airportOACI);
      cy.get('[data-cy=AirportFormOkBtn]').should('not.be.disabled');

      // IATA
      cy.get('[data-cy=AirportFormIataInput] input').clear();
      cy.get('[data-cy=AirportFormIataInput] input').type('a');
      cy.get('[data-cy=AirportFormIataInput] input').should('have.value', 'a');
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormIataInput] input').clear();
      cy.get('[data-cy=AirportFormIataInput] input').type('abc');
      cy.get('[data-cy=AirportFormIataInput] input').should('have.value', 'abc');
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormIataInput] input').clear();
      cy.get('[data-cy=AirportFormIataInput] input').type('ABCDE');
      cy.get('[data-cy=AirportFormIataInput] input').should('have.value', 'ABCDE');
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormIataInput] input').clear();
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormIataInput] input').type(airportIATA);
      cy.get('[data-cy=AirportFormOkBtn]').should('not.be.disabled');

      // NAME
      const nameOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
      cy.get('[data-cy=AirportFormNameInput] input').clear();
      cy.get('[data-cy=AirportFormNameInput] input').type(nameOver255);
      cy.get('[data-cy=AirportFormNameInput] input').should('have.value', nameOver255);
      cy.get('[data-cy=AirportFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=AirportFormNameInput] input').clear();
      cy.get('[data-cy=AirportFormNameInput] input').type(airportName);
      cy.get('[data-cy=AirportFormNameInput] input').should('have.value', airportName);
      cy.get('[data-cy=AirportFormOkBtn]').should('not.be.disabled');

      // Annuler
      cy.get('[data-cy=AirportFormCancelBtn]').click();
      cy.get('main .airvam-table tr:not(.disabled)').contains(airportName).should('not.exist');

      // Ajouter
      cy.intercept({
        method: 'POST',
        url: '/api/airport',
      }).as('createAirport');

      cy.get('[data-cy=AddAirportBtn]').click();
      cy.get('[data-cy=AirportFormOaciInput] input').type(airportOACI);
      cy.get('[data-cy=AirportFormIataInput] input').type(airportIATA);
      cy.get('[data-cy=AirportFormTimezoneInput] input').type(airportTimezone);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(airportTimezone).click();
      cy.get('[data-cy=AirportFormNameInput] input').type(airportName);
      cy.get('[data-cy=AirportFormOkBtn]').click();
      cy.wait('@createAirport').its('response.statusCode').should('equal', 201);

      cy.get('[data-cy=AirportListSearchInput] input').clear();
      cy.get('[data-cy=AirportListSearchInput] input').type(airportName);
      cy.wait(500);
      cy.get('main .airvam-table tr:not(.disabled)').contains(airportName).should('exist');
    });

    it(`Un super administrateur doit pouvoir éditer un code ICAO`, () => {
      cy.get('main .airvam-table tr:not(.disabled)').contains(airportName).parents('tr').find('[data-cy=EditAirportBtn]').click();
      cy.get('.v-dialog').contains(`Édition d'un aéroport`).should('exist');
      cy.get('[data-cy=AirportFormOaciInput] input').should('have.value', airportOACI);
      cy.get('[data-cy=AirportFormIataInput] input').should('have.value', airportIATA);
      cy.get('[data-cy=AirportFormTimezoneInput] .v-autocomplete__selection-text').contains(airportTimezone);
      cy.get('[data-cy=AirportFormNameInput] input').should('have.value', airportName);
      cy.get('[data-cy=AirportFormOkBtn]').should('not.be.disabled');

      cy.intercept({
        method: 'PATCH',
        url: '/api/airport/**',
      }).as('editAirport');
      cy.get('[data-cy=AirportFormIataInput] input').clear();
      cy.get('[data-cy=AirportFormIataInput] input').type('CBA');
      cy.get('[data-cy=AirportFormOkBtn]').click();
      cy.wait('@editAirport').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=AirportListSearchInput] input').clear();
      cy.get('[data-cy=AirportListSearchInput] input').type(airportName);
      cy.wait(500);
      cy.get('main .airvam-table tr:not(.disabled)').contains(airportName).should('exist');
    });

    it(`Un super administrateur doit pouvoir activer / désactiver un code ICAO`, () => {
      cy.get('[data-cy=AirportListSearchInput] input').type(airportName);
      cy.get('main .airvam-table tr:not(.disabled)').contains(airportName).parents('tr').find('[data-cy=DeleteAirportBtn]').click();
      cy.get('.v-dialog').contains(`Désactivation d'un aéroport`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'DELETE',
        url: '/api/airport/**',
      }).as('deleteAirport');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deleteAirport').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=AirportListSearchInput] input').clear();
      cy.get('[data-cy=AirportListSearchInput] input').type(airportName);
      cy.get('main .airvam-table tr.disabled').contains(airportName).should('exist');

      cy.get('main .airvam-table tr.disabled').contains(airportName).parents('tr').find('[data-cy=DeleteAirportBtn]').click();
      cy.get('.v-dialog').contains(`Activation d'un aéroport`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'PATCH',
        url: '/api/airport/**/activate',
      }).as('activateAirport');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@activateAirport').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=AirportListSearchInput] input').clear();
      cy.get('[data-cy=AirportListSearchInput] input').type(airportName);
      cy.get('main .airvam-table tr:not(.disabled)').contains(airportName).should('exist');
    });

    it(`Un super administrateur doit pouvoir naviguer dans la pagination`, () => {
      cy.get('.v-pagination').should('exist');
      cy.get('.v-pagination .v-pagination__item--is-active').contains('1').should('exist');

      cy.get('.v-pagination .v-pagination__item').contains('2').click();
      cy.get('.v-pagination .v-pagination__item--is-active').contains('2').should('exist');
    });
  });
});

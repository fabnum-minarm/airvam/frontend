describe('Administration des unités', () => {
  beforeEach(() => {
    cy.logout();
  });

  after(() => {
    cy.resetDb('unit');
  });

  it(`Un utilisateur ne doit pas pouvoir accéder à la gestion des unités`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/administration');
    cy.url().should('match', /vols/)
  });

  it(`Un administrateur ne doit pas pouvoir accéder à la gestion des unités`, () => {
    cy.devLogin(Cypress.env('devTestAdmin'));
    cy.visit('/administration');
    cy.get('main .v-tabs').contains('Unités').should('not.exist');
  });

  describe('Administration des unités - Super admin', () => {
    beforeEach(() => {
      cy.devLogin(Cypress.env('devTestGod'));
      cy.visit('/administration');
      cy.get('main .v-tabs').contains('Unités').click();
    });

    it(`Un super administrateur doit pouvoir accéder à la gestion des unités`, () => {
      cy.get('main .v-tabs').contains('Unités').should('exist');
      cy.get('[data-cy=AddUnitBtn]').should('exist');
      cy.get('main .airvam-table').contains('Dénomination').should('exist');
    });

    const unitCode = `CYTEST${Math.floor(Math.random() * 10000)}`;
    const unitName = `CYTEST${Math.floor(Math.random() * 10000)}`;
    const unitEmail = 'email@example.com';
    const unitPhone = '0606060606';
    const airports = ['LFMI', 'LFPC'];
    it(`Un super administrateur doit pouvoir ajouter une unité`, () => {
      cy.get('[data-cy=AddUnitBtn]').click();
      cy.get('.v-dialog').contains(`Création d'une unité`).should('exist');
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      // Si le formulaire n'est pas valide, même au click forcé sur le bouton il ne doit rien se passer
      cy.get('[data-cy=UnitFormOkBtn]').invoke('removeAttr', 'disabled');
      cy.get('[data-cy=UnitFormOkBtn]').invoke('removeClass', 'v-btn--disabled');
      cy.get('[data-cy=UnitFormOkBtn]').click();
      cy.wait(500);
      cy.get('.v-dialog').contains(`Création d'une unité`).should('exist');
      cy.get('[data-cy=UnitFormCancelBtn]').click();
      cy.get('[data-cy=AddUnitBtn]').click();

      cy.get('[data-cy=UnitFormCodeInput] input').type(unitCode);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UnitFormNameInput] input').type(unitName);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=UnitFormAirportsInput] input').type(airports[0], {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(airports[0]).click();
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');
      cy.get('[data-cy=UnitFormAirportsInput] input').type(airports[1], {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(airports[1]).click();
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Test de la validité du formulaire
      const stringOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
      // Code
      cy.get('[data-cy=UnitFormCodeInput] input').clear();
      cy.get('[data-cy=UnitFormCodeInput] input').type(stringOver255);
      cy.get('[data-cy=UnitFormCodeInput] input').should('have.value', stringOver255);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormCodeInput] input').clear();
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormCodeInput] input').type(unitCode);
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Name
      cy.get('[data-cy=UnitFormNameInput] input').clear();
      cy.get('[data-cy=UnitFormNameInput] input').type(stringOver255);
      cy.get('[data-cy=UnitFormNameInput] input').should('have.value', stringOver255);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormNameInput] input').clear();
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormNameInput] input').type(unitCode);
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Address1
      cy.get('[data-cy=UnitFormAddress1Input] input').clear();
      cy.get('[data-cy=UnitFormAddress1Input] input').type(stringOver255);
      cy.get('[data-cy=UnitFormAddress1Input] input').should('have.value', stringOver255);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormAddress1Input] input').clear();
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Address2
      cy.get('[data-cy=UnitFormAddress2Input] input').clear();
      cy.get('[data-cy=UnitFormAddress2Input] input').type(stringOver255);
      cy.get('[data-cy=UnitFormAddress2Input] input').should('have.value', stringOver255);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormAddress2Input] input').clear();
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Address3
      cy.get('[data-cy=UnitFormAddress3Input] input').clear();
      cy.get('[data-cy=UnitFormAddress3Input] input').type(stringOver255);
      cy.get('[data-cy=UnitFormAddress3Input] input').should('have.value', stringOver255);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormAddress3Input] input').clear();
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Phone Number
      cy.get('[data-cy=UnitFormPhoneInput] input').clear();
      cy.get('[data-cy=UnitFormPhoneInput] input').type(stringOver255);
      cy.get('[data-cy=UnitFormPhoneInput] input').should('have.value', stringOver255);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormPhoneInput] input').clear();
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Email
      cy.get('[data-cy=UnitFormEmailInput] input').clear();
      cy.get('[data-cy=UnitFormEmailInput] input').type(`${stringOver255}@example.com`);
      cy.get('[data-cy=UnitFormEmailInput] input').should('have.value', `${stringOver255}@example.com`);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormEmailInput] input').clear();
      cy.get('[data-cy=UnitFormEmailInput] input').type(`NOT A MAIL`);
      cy.get('[data-cy=UnitFormEmailInput] input').should('have.value', `NOT A MAIL`);
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=UnitFormEmailInput] input').clear();
      cy.get('[data-cy=UnitFormEmailInput] input').type(unitEmail);
      cy.get('[data-cy=UnitFormEmailInput] input').should('have.value', `email@example.com`);
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      cy.get('[data-cy=UnitFormEmailInput] input').clear();
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      // Airports
      cy.get('.v-input .v-chip .v-chip__close').first().click();
      cy.get('.v-input .v-chip .v-chip__close').first().click();
      cy.get('[data-cy=UnitFormOkBtn]').should('be.disabled');

      // Annuler
      cy.get('[data-cy=UnitFormCancelBtn]').click();
      cy.get('main .airvam-table tr:not(.disabled)').contains(unitName).should('not.exist');

      // Ajouter
      cy.intercept({
        method: 'POST',
        url: '/api/unit',
      }).as('createUnit');

      cy.get('[data-cy=AddUnitBtn]').click();
      cy.get('[data-cy=UnitFormCodeInput] input').type(unitCode);
      cy.get('[data-cy=UnitFormNameInput] input').type(unitName);
      cy.get('[data-cy=UnitFormAddress1Input] input').type(unitName);
      cy.get('[data-cy=UnitFormAddress2Input] input').type(unitName);
      cy.get('[data-cy=UnitFormAddress3Input] input').type(unitName);
      cy.get('[data-cy=UnitFormPhoneInput] input').type(unitPhone);
      cy.get('[data-cy=UnitFormEmailInput] input').type(unitEmail);
      cy.get('[data-cy=UnitFormAirportsInput] input').type(airports[0], {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(airports[0]).click();
      cy.get('[data-cy=UnitFormAirportsInput] input').type(airports[1], {force: true});
      cy.get(".v-overlay-container .v-list .v-list-item").contains(airports[1]).click();
      cy.get('[data-cy=UnitFormOkBtn]').click();
      cy.wait('@createUnit').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(unitName).should('exist');
    });

    it(`Un super administrateur doit pouvoir éditer une unité`, () => {
      cy.get('main .airvam-table tr:not(.disabled)').contains(unitName).parents('tr').find('[data-cy=EditUnitBtn]').click();
      cy.get('.v-dialog').contains(`Édition d'une unité`).should('exist');
      cy.get('[data-cy=UnitFormCodeInput] input').should('have.value', unitCode);
      cy.get('[data-cy=UnitFormNameInput] input').should('have.value', unitName);
      cy.get('[data-cy=UnitFormAddress1Input] input').should('have.value', unitName);
      cy.get('[data-cy=UnitFormAddress2Input] input').should('have.value', unitName);
      cy.get('[data-cy=UnitFormAddress3Input] input').should('have.value', unitName);
      // cy.get('[data-cy=UnitFormPhoneInput] input').should('have.value', unitPhone);
      cy.get('[data-cy=UnitFormEmailInput] input').should('have.value', unitEmail);
      cy.get('[data-cy=UnitFormAirportsInput] input').should('exist');
      cy.get('[data-cy=UnitFormAirportsInput] input').parents('.v-input').find('.v-chip').should('have.length', 2);
      cy.get('[data-cy=UnitFormOkBtn]').should('not.be.disabled');

      cy.intercept({
        method: 'PATCH',
        url: '/api/unit/**',
      }).as('editUnit');
      cy.get('[data-cy=UnitFormCodeInput] input').clear();
      cy.get('[data-cy=UnitFormCodeInput] input').type('CODE 01');
      cy.get('[data-cy=UnitFormOkBtn]').click();
      cy.wait('@editUnit').its('response.statusCode').should('equal', 200);
      cy.get('main .airvam-table tr:not(.disabled)').contains(unitName).should('exist');
    });

    it(`Un super administrateur doit pouvoir activer / désactiver une unité`, () => {
      cy.get('[data-cy=UnitListSearchInput] input').type(unitName);
      cy.get('main .airvam-table tr:not(.disabled)').contains(unitName).parents('tr').find('[data-cy=DeleteUnitBtn]').click();
      cy.get('.v-dialog').contains(`Désactivation d'une unité`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'DELETE',
        url: '/api/unit/**',
      }).as('deleteUnit');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deleteUnit').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=UnitListSearchInput] input').clear();
      cy.get('[data-cy=UnitListSearchInput] input').type(unitName);
      cy.get('main .airvam-table tr.disabled').contains(unitName).should('exist');

      cy.get('main .airvam-table tr.disabled').contains(unitName).parents('tr').find('[data-cy=DeleteUnitBtn]').click();
      cy.get('.v-dialog').contains(`Activation d'une unité`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'PATCH',
        url: '/api/unit/**/activate',
      }).as('activateUnit');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@activateUnit').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=UnitListSearchInput] input').clear();
      cy.get('[data-cy=UnitListSearchInput] input').type(unitName);
      cy.get('main .airvam-table tr:not(.disabled)').contains(unitName).should('exist');
    });
  });
});

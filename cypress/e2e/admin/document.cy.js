describe('Administration des documents', () => {
  before(() => {
    cy.logout();
  });

  after(() => {
    cy.resetDb('document');
  });

  it(`Un utilisateur ne doit pas pouvoir accéder à la gestion des documents`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/administration');
    cy.url().should('match', /vols/)
  });

  it(`Un administrateur ne doit pas pouvoir accéder à la gestion des documents`, () => {
    cy.devLogin(Cypress.env('devTestAdmin'));
    cy.visit('/administration');
    cy.get('main .v-tabs').contains('Documents').should('not.exist');
  });

  describe('Administration des documents - Super admin', () => {
    beforeEach(() => {
      cy.devLogin(Cypress.env('devTestGod'));
      cy.visit('/administration');
      cy.get('main .v-tabs').contains('Documents').click();
    });

    it(`Un super administrateur doit pouvoir accéder à la gestion des documents`, () => {
      cy.get('main .v-tabs').contains('Documents').should('exist');
      cy.get('[data-cy=AddDocumentBtn]').should('exist');
      cy.get('main .airvam-table').contains('Catégorie').should('exist');
    });

    const docCategory = 'DOC_CAT' ;
    const docName = `CYTEST_F-UJCU.pdf`;
    const docNameEdit = `CYTEST${Math.floor(Math.random() * 1000)}.pdf`;
    it(`Un super administrateur doit pouvoir ajouter un document`, () => {
      cy.get('[data-cy=AddDocumentBtn]').click();
      cy.get('.v-overlay-container .v-dialog').contains(`Création d'un document`).should('exist');
      cy.get('[data-cy=DocumentFormOkBtn]').should('be.disabled');

      // Si le formulaire n'est pas valide, même au click forcé sur le bouton il ne doit rien se passer
      cy.get('[data-cy=DocumentFormOkBtn]').invoke('removeAttr', 'disabled');
      cy.get('[data-cy=DocumentFormOkBtn]').invoke('removeClass', 'v-btn--disabled');
      cy.get('[data-cy=DocumentFormOkBtn]').click();
      cy.wait(500);
      cy.get('.v-overlay-container .v-dialog').contains(`Création d'un document`).should('exist');
      cy.get('[data-cy=DocumentFormCancelBtn]').click();
      cy.get('[data-cy=AddDocumentBtn]').click();

      cy.get('[data-cy=DocumentFormNameInput] input').should('not.exist');

      cy.get('[data-cy=DocumentFormDocumentInput] input').selectFile(`cypress/fixtures/${docName}`, {
        action: 'drag-drop',
        force: true
      });
      cy.get('[data-cy=DocumentFormOkBtn]').should('not.be.disabled');

      // Test de la validité du formulaire
      const stringOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
      cy.get('[data-cy=DocumentFormCategoryInput] input').type(stringOver255);
      cy.get('[data-cy=DocumentFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=DocumentFormCategoryInput] input').clear();
      cy.get('[data-cy=DocumentFormOkBtn]').should('not.be.disabled');

      cy.get('[data-cy=DocumentFormDocumentInput] .v-field__clearable i').click();
      cy.get('[data-cy=DocumentFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=DocumentFormDocumentInput] input').selectFile(`cypress/fixtures/${docName}`, {
        action: 'drag-drop',
        force: true
      });
      cy.get('[data-cy=DocumentFormCategoryInput] input').type(docCategory);
      cy.get('[data-cy=DocumentFormOkBtn]').should('not.be.disabled');

      // Annuler
      cy.get('[data-cy=DocumentFormCancelBtn]').click();
      cy.get('main .airvam-table tr').contains(docName).should('not.exist');

      // Ajouter
      cy.intercept({
        method: 'POST',
        url: '/api/document',
      }).as('createDocument');

      cy.get('[data-cy=AddDocumentBtn]').click();
      cy.get('[data-cy=DocumentFormDocumentInput] input').selectFile(`cypress/fixtures/${docName}`, {
        action: 'drag-drop',
        force: true
      });
      cy.get('[data-cy=DocumentFormCategoryInput] input').type(docCategory);
      cy.get('[data-cy=DocumentFormOkBtn]').click();
      cy.wait('@createDocument').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr').contains(docName).should('exist');
    });

    it(`Un super administrateur doit pouvoir éditer un document`, () => {
      cy.get('main .airvam-table tr').contains(docName).parents('tr').find('[data-cy=EditDocumentBtn]').click();
      cy.get('.v-dialog').contains(`Édition d'un document`).should('exist');
      cy.get('[data-cy=DocumentFormDocumentInput] input').should('not.exist');
      cy.get('[data-cy=DocumentFormNameInput] input').should('have.value', docName);
      cy.get('[data-cy=DocumentFormCategoryInput] input').should('have.value', docCategory);
      cy.get('[data-cy=DocumentFormOkBtn]').should('not.be.disabled');

      cy.intercept({
        method: 'PATCH',
        url: '/api/document/**',
      }).as('editDocument');
      cy.get('[data-cy=DocumentFormNameInput] input').clear();
      cy.get('[data-cy=DocumentFormNameInput] input').type(docNameEdit);
      cy.get('[data-cy=DocumentFormOkBtn]').click();
      cy.wait('@editDocument').its('response.statusCode').should('equal', 200);
      cy.get('main .airvam-table tr').contains(docNameEdit).should('exist');
    });

    it(`Un super administrateur doit pouvoir supprimer un document`, () => {
      cy.get('[data-cy=DocumentListSearchInput] input').type(docNameEdit);
      cy.get('main .airvam-table tr').contains(docNameEdit).parents('tr').find('[data-cy=DeleteDocumentBtn]').click();
      cy.get('.v-dialog').contains(`Suppression d'un document`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'DELETE',
        url: '/api/document/**',
      }).as('deleteDocument');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deleteDocument').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=DocumentListSearchInput] input').clear();
      cy.get('[data-cy=DocumentListSearchInput] input').type(docNameEdit);
      cy.get('main .airvam-table tr').contains(docNameEdit).should('not.exist');
    });
  });
});

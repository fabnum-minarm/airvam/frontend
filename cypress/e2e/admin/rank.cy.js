describe('Administration des grades', () => {
  before(() => {
    cy.logout();
  });

  after(() => {
    cy.resetDb('rank');
  });

  it(`Un utilisateur ne doit pas pouvoir accéder à la gestion des grades`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/administration');
    cy.url().should('match', /vols/)
  });

  it(`Un administrateur ne doit pas pouvoir accéder à la gestion des grades`, () => {
    cy.devLogin(Cypress.env('devTestAdmin'));
    cy.visit('/administration');
    cy.get('main .v-tabs').contains('Grades').should('not.exist');
  });

  describe('Administration des grades - Super admin', () => {
    beforeEach(() => {
      cy.devLogin(Cypress.env('devTestGod'));
      cy.visit('/administration');
      cy.get('main .v-tabs').contains('Grades').click();
    });

    it(`Un super administrateur doit pouvoir accéder à la gestion des grades`, () => {
      cy.get('main .v-tabs').contains('Grades').should('exist');
      cy.get('[data-cy=AddRankBtn]').should('exist');
      cy.get('main .airvam-table').contains('Grade').should('exist');
    });

    const rankName = `CYTEST${Math.floor(Math.random() * 1000)}`;
    const rankRank = '100';
    const rankCategory = 'Air et Espace' ;
    it(`Un super administrateur doit pouvoir ajouter un grade`, () => {
      cy.get('[data-cy=AddRankBtn]').click();
      cy.get('.v-dialog').contains(`Création d'un grade`).should('exist');
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');

      // Si le formulaire n'est pas valide, même au click forcé sur le bouton il ne doit rien se passer
      cy.get('[data-cy=RankFormOkBtn]').invoke('removeAttr', 'disabled');
      cy.get('[data-cy=RankFormOkBtn]').invoke('removeClass', 'v-btn--disabled');
      cy.get('[data-cy=RankFormOkBtn]').click();
      cy.wait(500);
      cy.get('.v-dialog').contains(`Création d'un grade`).should('exist');
      cy.get('[data-cy=RankFormCancelBtn]').click();
      cy.get('[data-cy=AddRankBtn]').click();

      cy.get('[data-cy=RankFormRankInput] input').type('1');
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=RankFormNameInput] input').type(rankName);
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=RankFormCategoryInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(rankCategory).click();
      cy.get('[data-cy=RankFormOkBtn]').should('not.be.disabled');

      // Test de la validité du formulaire
      // ORDRE
      cy.get('[data-cy=RankFormRankInput] input').clear();
      cy.get('[data-cy=RankFormRankInput] input').type('a');
      cy.get('[data-cy=RankFormRankInput] input').should('have.value', '');

      cy.get('[data-cy=RankFormRankInput] input').clear();
      cy.get('[data-cy=RankFormRankInput] input').type('-1');
      cy.get('[data-cy=RankFormRankInput] input').should('have.value', '-1');
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=RankFormRankInput] input').clear();
      cy.get('[data-cy=RankFormRankInput] input').type('101');
      cy.get('[data-cy=RankFormRankInput] input').should('have.value', '101');
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=RankFormRankInput] input').clear();
      cy.get('[data-cy=RankFormRankInput] input').type('0');
      cy.get('[data-cy=RankFormRankInput] input').should('have.value', '0');
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=RankFormRankInput] input').clear();
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=RankFormRankInput] input').type(rankRank);
      cy.get('[data-cy=RankFormOkBtn]').should('not.be.disabled');

      // GRADE
      cy.get('[data-cy=RankFormNameInput] input').clear();
      cy.get('[data-cy=RankFormNameInput] input').type('ABCDEFGHIJK');
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=RankFormNameInput] input').should('have.value', 'ABCDEFGHIJK');

      cy.get('[data-cy=RankFormNameInput] input').clear();
      cy.get('[data-cy=RankFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=RankFormNameInput] input').type(rankName);
      cy.get('[data-cy=RankFormOkBtn]').should('not.be.disabled');

      // Annuler
      cy.get('[data-cy=RankFormCancelBtn]').click();
      cy.get('main .airvam-table tr:not(.disabled)').contains(rankName).should('not.exist');

      // Ajouter
      cy.intercept({
        method: 'POST',
        url: '/api/rank',
      }).as('createRank');

      cy.get('[data-cy=AddRankBtn]').click();
      cy.get('[data-cy=RankFormNameInput] input').type(rankName);
      cy.get('[data-cy=RankFormRankInput] input').type(rankRank);
      cy.get('[data-cy=RankFormCategoryInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").first().click();
      cy.get('[data-cy=RankFormOkBtn]').click();
      cy.wait('@createRank').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(rankName).should('exist');
    });

    it(`Un super administrateur doit pouvoir éditer un grade`, () => {
      cy.get('main .airvam-table tr:not(.disabled)').contains(rankName).parents('tr').find('[data-cy=EditRankBtn]').click();
      cy.get('.v-dialog').contains(`Édition d'un grade`).should('exist');
      cy.get('[data-cy=RankFormRankInput] input').should('have.value', rankRank);
      cy.get('[data-cy=RankFormNameInput] input').should('have.value', rankName);
      cy.get('[data-cy=RankFormCategoryInput] input').should('exist');
      cy.get('[data-cy=RankFormCategoryInput] input').parents('.v-input').find('.v-select__selection').should('have.text', rankCategory);
      cy.get('[data-cy=RankFormOkBtn]').should('not.be.disabled');

      cy.intercept({
        method: 'PATCH',
        url: '/api/rank/**',
      }).as('editRank');
      cy.get('[data-cy=RankFormRankInput] input').clear();
      cy.get('[data-cy=RankFormRankInput] input').type('1');
      cy.get('[data-cy=RankFormOkBtn]').click();
      cy.wait('@editRank').its('response.statusCode').should('equal', 200);
      cy.get('main .airvam-table tr:not(.disabled)').contains(rankName).should('exist');
    });

    it(`Un super administrateur doit pouvoir activer / désactiver un grade`, () => {
      cy.get('[data-cy=RankListSearchInput] input').type(rankName);
      cy.get('main .airvam-table tr:not(.disabled)').contains(rankName).parents('tr').find('[data-cy=DeleteRankBtn]').click();
      cy.get('.v-dialog').contains(`Désactivation d'un grade`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'DELETE',
        url: '/api/rank/**',
      }).as('deleteRank');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@deleteRank').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=RankListSearchInput] input').clear();
      cy.get('[data-cy=RankListSearchInput] input').type(rankName);
      cy.get('main .airvam-table tr.disabled').contains(rankName).should('exist');

      cy.get('main .airvam-table tr.disabled').contains(rankName).parents('tr').find('[data-cy=DeleteRankBtn]').click();
      cy.get('.v-dialog').contains(`Activation d'un grade`).should('exist');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

      cy.intercept({
        method: 'PATCH',
        url: '/api/rank/**/activate',
      }).as('activateRank');
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
      cy.wait('@activateRank').its('response.statusCode').should('equal', 200);

      cy.get('[data-cy=RankListSearchInput] input').clear();
      cy.get('[data-cy=RankListSearchInput] input').type(rankName);
      cy.get('main .airvam-table tr:not(.disabled)').contains(rankName).should('exist');
    });
  });
});

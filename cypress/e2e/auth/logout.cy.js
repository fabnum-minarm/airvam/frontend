describe('Test du logout', () => {
  beforeEach(() => {
    cy.logout();
  });

  it(`Nous devons être redirigé vers la page de login, le header ne doit pas être visible`, () => {
    cy.visit('/');
    cy.location('pathname').should('equal', '/login');
    cy.get('header').should('not.exist');
  });

  const users = [Cypress.env('devTestUser'), Cypress.env('devTestAdmin'), Cypress.env('devTestGod')];
  users.forEach(userEmail => {
    it(`Au login, le header doit être visible avec la possibilité de se déconnecter (${userEmail})`, () => {
      cy.devLogin(userEmail);
      cy.get('header').should('exist');
      cy.get('header .v-avatar').click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains('Déconnexion').click();
      cy.url().should('include', '/login');
      cy.visit('/vols');
      cy.url().should('include', '/login');
    });
  });
});

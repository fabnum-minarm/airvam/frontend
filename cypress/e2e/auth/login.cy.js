describe('Test de la page de Login', () => {
  beforeEach(() => {
    cy.logout();
  });

  it(`Nous devons être redirigé vers la page de login`, () => {
    cy.visit('/');
    cy.location('pathname').should('equal', '/login');
  });

  describe('Login de dev', () => {
    it(`Le bouton "connecté" doit être disabled si aucun utilisateur n'est sélectionné`, () => {
      cy.get('[data-cy=LoginDevBtn]').should('be.disabled');
    });

    it('Test du login', () => {
      cy.get('[data-cy=LoginDevUserList]').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").first().click();
      cy.get('[data-cy=LoginDevBtn]').should('not.be.disabled');
      cy.get('[data-cy=LoginDevBtn]').click();
      cy.url().should('include', '/vols');

      describe('La page de login ne doit plus être accessible', () => {
        cy.visit('/login');
        cy.url().should('include', '/vols');
      });
    });
  });
});

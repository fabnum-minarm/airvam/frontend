describe('Gestion des rôles', () => {
  beforeEach(() => {
    cy.logout();
  });

  it(`Un utilisateur ne doit pas pouvoir accéder à la partie administration`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.get('header').should('exist');
    cy.get('header').contains('Administration').should('not.exist');
    cy.visit('/administration');
    cy.url().should('include', '/vols');
  });

  const users = [Cypress.env('devTestAdmin'), Cypress.env('devTestGod')];
  users.forEach(userEmail => {
    it(`Un administrateur et un super administrateur doivent pouvoir accéder à la partie administration (${userEmail})`, () => {
      cy.devLogin(userEmail);
      cy.get('header').should('exist');
      cy.get('header').contains('Administration').should('exist');
      cy.get('header').contains('Administration').click();
      cy.url().should('include', '/administration');
    });
  });

});

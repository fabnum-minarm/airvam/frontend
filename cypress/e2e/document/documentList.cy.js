describe('Liste des documents', () => {

  const docCategory = 'DOC_CAT' ;
  const docName = `CYTEST_F-UJCU.pdf`;

  before(() => {
    cy.devLogin(Cypress.env('devTestGod'));
    cy.visit('/administration');
    cy.get('main .v-tabs').contains('Documents').click();
    // Ajout de deux documents de test
    cy.intercept({
      method: 'POST',
      url: '/api/document',
    }).as('createDocument');

    cy.get('[data-cy=AddDocumentBtn]').click();
    cy.get('[data-cy=DocumentFormDocumentInput] input').selectFile(`cypress/fixtures/${docName}`, {
      action: 'drag-drop',
      force: true
    });
    cy.get('[data-cy=DocumentFormCategoryInput] input').type(docCategory);
    cy.get('[data-cy=DocumentFormOkBtn]').click();
    cy.wait('@createDocument').its('response.statusCode').should('equal', 201);

    cy.get('[data-cy=AddDocumentBtn]').click();
    cy.get('[data-cy=DocumentFormDocumentInput] input').selectFile(`cypress/fixtures/${docName}`, {
      action: 'drag-drop',
      force: true
    });
    cy.get('[data-cy=DocumentFormOkBtn]').click();
    cy.wait('@createDocument').its('response.statusCode').should('equal', 201);
  });

  after(() => {
    cy.resetDb('document');
  });

  it(`Un utilisateur doit pouvoir accéder à la liste des documents`, () => {
    // Je sais pas pq ça veut pas se log spécifiquement sur le premier test :(
    cy.devLogin(Cypress.env('devTestUser'));
    cy.url().should('match', /vols/);
    cy.get('header').contains('Documents').click();
    cy.url().should('include', '/documents');
  });

  it(`Un utilisateur doit pouvoir rechercher un document`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/documents');
    cy.url().should('include', '/documents');

    cy.get('[data-cy=DocumentListSearchInput] input').clear();
    cy.get('[data-cy=DocumentListSearchInput] input').type(docCategory);
    cy.get('main .airvam-table tr').contains(docCategory).should('exist');
    cy.get('main .airvam-table td').contains(docName).should('not.exist');
    cy.get('main .airvam-table tr').contains(docCategory).click();
    cy.get('main .airvam-table td').contains(docName).should('exist');
  });

  it(`Un utilisateur doit pouvoir télécharger un document`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/documents');
    cy.url().should('include', '/documents');

    cy.get('main .airvam-table tr').contains('Aucune catégorie').should('exist');
    cy.get('main .airvam-table tr').contains('Aucune catégorie').click();
    cy.get('main .airvam-table td').contains(docName).should('exist');
    cy.get('main .airvam-table td').contains(docName).parent().find('[data-cy=DocumentListDownloadBtn]').should('exist');
    cy.get('main .airvam-table td').contains(docName).parent().find('[data-cy=DocumentListDownloadBtn]').click();
    cy.verifyDownload(docName);
  });
});
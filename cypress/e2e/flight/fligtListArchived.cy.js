describe('Liste des vols archivés', () => {
  before(() => {
    cy.logout();
    cy.populateDb('user');
    cy.populateDb('flight');
  });

  after(() => {
    cy.resetDb('flight');
  });

  it(`Un utilisateur doit pouvoir voir et accéder aux vols archivés de son scope uniquement`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.get('header').contains('Vols archivés').click();
    cy.url().should('include', '/vols_archives');

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type('CYTEST_');
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_OACI_NAME').should('not.exist');
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').should('exist');

    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').parent('tr').click();
    cy.url().should('match', /vols\/\d+\/edition/);
  });

  it(`Un administrateur doit pouvoir voir et accéder aux vols archivés de son scope uniquement`, () => {
    cy.devLogin(Cypress.env('devTestAdmin'));
    cy.get('header').contains('Vols archivés').click();
    cy.url().should('include', '/vols_archives');

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type('CYTEST_');
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_OACI_NAME').should('not.exist');
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').should('exist');
  });

  it(`Un super administrateur doit pouvoir voir et accéder à tout les vols archivés`, () => {
    cy.devLogin(Cypress.env('devTestGod'));
    cy.get('header').contains('Vols archivés').click();
    cy.url().should('include', '/vols_archives');

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type('CYTEST_');
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_OACI_NAME').should('exist');
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').should('exist');
  });

  it(`Un vol archivé doit s'afficher correctement dans le listing`, () => {
    cy.devLogin(Cypress.env('devTestGod'));
    cy.get('header').contains('Vols archivés').click();
    cy.url().should('include', '/vols_archives');

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type('CYTEST_');
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);

    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').should('exist');
    const singleLeg = cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').parent('tr');
    singleLeg.contains('CYTEST_SINGLE_LEG_NUMBER').should('exist');
    singleLeg.parent('tr').contains('CYTEST_SINGLE_LEG_COTAM').should('exist');
    singleLeg.parent('tr').contains('A330 F-UJCT').should('exist');
    singleLeg.parent('tr').contains('LFMI - LFPC').should('exist');
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').parent('tr').contains('À préparer').should('exist');

    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_TWO_LEGS_NAME').should('exist');
    const twoLegs = cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_TWO_LEGS_NAME').parent('tr');
    twoLegs.contains('CYTEST_TWO_LEGS_NUMBER').should('exist');
    twoLegs.parent('tr').contains('CYTEST_TWO_LEGS_COTAM').should('exist');
    twoLegs.parent('tr').contains('A330 F-UJCS').should('exist');
    twoLegs.parent('tr').contains('LFMI - LFMI').should('exist');
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_TWO_LEGS_NAME').parent('tr').contains('À préparer').should('exist');
  });

  it(`Un utilisateur doit pouvoir voir désarchiver un vol`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.get('header').contains('Vols archivés').click();
    cy.url().should('include', '/vols_archives');

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type('CYTEST_SINGLE_LEG_NAME');
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('.airvam-table tbody').find('tr').should('have.length', 1);
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').should('exist');
    cy.get('[data-cy=ArchiveFlightBtn]').click();

    cy.get('.v-dialog').contains(`Désarchiver un vol`).should('exist');
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').should('exist');

    cy.intercept({
      method: 'PATCH',
      url: '/api/flight/**/activate',
    }).as('activateFlight');
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.wait('@activateFlight').its('response.statusCode').should('equal', 200);

    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type('CYTEST_SINGLE_LEG_NAME');
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').should('not.exist');

    cy.get('header').contains('Vols en prévision').click();
    cy.url().should('include', '/vols');

    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type('CYTEST_SINGLE_LEG_NAME');
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains('CYTEST_SINGLE_LEG_NAME').should('exist');
  });

});

describe('Parcours complet vol avec un seul leg', () => {
  before(() => {
    cy.logout();
    cy.populateDb('user');
  });

  after(() => {
    cy.resetDb('flight');
  });

  const flightName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const flightAircraft = 'F-UJCT';
  const legDeparture = 'LFMI';
  const legArrival = 'LFPC';

  function goToFlightPage() {
    cy.devLogin(Cypress.env('devTestUser'));

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input', {timeout: 10000}).clear();
    cy.get('[data-cy=FlightListSearchInput] input').type(flightName);
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains(flightName).click();
    cy.url().should('match', /vols\/\d+\/edition/);
    cy.get('[data-cy=FlightTitleHeader]').contains(`Vol ${flightName} / ${legDeparture} - ${legArrival}`).should('exist');
  }

  it(`Création du vol`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/vols/nouveau/edition');

    // remplissage du formulaire
    cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightName);
    cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(flightAircraft).click();
    cy.get('[data-cy=FlightFormLegUserInput] input').type(flightName);

    // remplissage d'un leg
    cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
    cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
    cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
    cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
    cy.get('.v-overlay-container .v-btn.text-green').click();

    cy.intercept({
      method: 'POST',
      url: '/api/flight',
    }).as('flightCreate');
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
    cy.url().should('match', /vols\/\d+\/edition/);
  });

  it(`Ajout des PAX`, () => {
    goToFlightPage();

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);

    // cy.get('[data-cy=PaxDownloadFileBtn]').click();
    // cy.verifyDownload('Modele_fichier_PAX.xlsx');

    cy.intercept({
      method: 'POST',
      url: '/api/pax/parse/**',
    }).as('paxParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/PAX_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@paxParse').its('response.statusCode').should('equal', 201);

    cy.get('[data-cy=PaxListErrorsFilterBtn]').parent().click();
    cy.get('[data-cy=PaxDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=PaxDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

    cy.get('[data-cy=PaxListWarningsFilterBtn]').parent().click();
    cy.get('[data-cy=PaxDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201)
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);
  });

  it(`Ajout des Animaux`, () => {
    goToFlightPage();

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseAnimalListBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/animal/);

    cy.get('.airvam-table').contains('La liste des animaux est vide').should('exist');
    cy.get('[data-cy=AnimalListErrorsFilterBtn]').should('not.exist');
    cy.get('[data-cy=AnimalListWarningsFilterBtn]').should('not.exist');
    cy.get('[data-cy=AnimalListDeleteBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalImportSaveBtn]').should('be.disabled');


    cy.intercept({
      method: 'POST',
      url: '/api/animal/parse/**',
    }).as('animalParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/ANIMAL_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@animalParse').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=AnimalListCountBadge]').should('contain', '7');
    cy.get('[data-cy=AnimalListErrorsBadge]').should('contain', '2');
    cy.get('[data-cy=AnimalListWarningsBadge]').should('contain', '2');
    cy.get('.airvam-table tbody').find('tr').should('have.length', 7);

    cy.get('[data-cy=AnimalListErrorsFilterBtn]').parent().click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 2);
    cy.get('[data-cy=AnimalDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=AnimalListErrorsBadge]').should('contain', '1');
    cy.get('[data-cy=AnimalDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=AnimalListErrorsBadge]').should('not.exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);

    cy.get('[data-cy=AnimalListWarningsFilterBtn]').parent().click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 2);
    cy.get('[data-cy=AnimalDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=AnimalListWarningsBadge]').should('not.exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);

    cy.intercept({
      method: 'POST',
      url: '/api/animal/updateList/**',
    }).as('animalSave');
    cy.get('[data-cy=AnimalImportSaveBtn]').click();
    cy.wait('@animalSave').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=AnimalImportSaveBtn]').should('be.disabled');
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);
  });

  it(`Génération du plan cabine`, () => {
    goToFlightPage();

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseSeatsBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/plan_cabine/);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Incomplet').should('exist');

    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('be.disabled');
    cy.get('.balancy-zones').contains(`TOTAL PAX: 17`).should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 0);

    cy.get('[data-cy=SeatsBalancingRadioExternalBtn]').parent().click();

    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/autoPlacement',
    }).as('legAutoPlacement');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);

    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 17);
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('not.be.disabled');

    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/finalize',
    }).as('legFinalize');
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');
  });
});

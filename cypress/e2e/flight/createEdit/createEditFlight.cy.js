describe('Création vol', () => {
  before(() => {
    cy.logout();
    cy.populateDb('user');
  });

  after(() => {
    cy.resetDb('flight');
  });

  it(`Un utilisateur ne doit pas pouvoir accéder à la création d'un vol sur la page d'archive`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/vols_archives');
    cy.get('[data-cy=AddFlightBtn]').should('not.exist');
  });

  it(`Un utilisateur doit pouvoir créer un vol`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/vols');
    cy.get('[data-cy=AddFlightBtn]').click();
    cy.url().should('match', /vols\/nouveau\/edition/);
  });

  it(`Quand un vol est en cours d'édition, une popin doit apparaître avant de quitter l'écran`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/vols');
    cy.get('[data-cy=AddFlightBtn]').click();

    cy.get('[data-cy=FlightFormMissionNameInput] input').type('TEST');

    cy.get('[data-cy=ReturnFlightHeaderBtn]').click();
    cy.url().should('match', /vols\/nouveau\/edition/);
    cy.get('[data-cy=ConfirmDialogCancelBtn]').click();
    cy.url().should('match', /vols\/nouveau\/edition/);
    cy.get('[data-cy=ReturnFlightHeaderBtn]').click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.url().should('match', /vols/);
  });

  describe(`Création d'un vol`, () => {
    beforeEach(() => {
      cy.devLogin(Cypress.env('devTestUser'));
      cy.visit('/vols/nouveau/edition');
    });

    const textOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
    const flightName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
    const flightLegsName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
    const flightFSName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
    const flightAircraft = 'F-UJCT' ;
    const legDeparture = 'LFMI';
    const legArrival = 'LFPC';

    it(`Avec un seul leg`, () => {
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');

      // remplissage du formulaire
      cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightName);
      cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightName);
      cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightName);
      cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(flightAircraft).click();
      cy.get('[data-cy=FlightFormLegUserInput] input').type(flightName);

      // remplissage d'un leg
      cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
      cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
      cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      cy.get('[data-cy=FlightFormOkBtn]').should('not.be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('not.be.disabled');

      cy.get('[data-cy=FlightFormMissionNameInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormMissionNameInput] input').type(textOver255);
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormMissionNameInput] input').clear();
      cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightName);

      cy.get('[data-cy=FlightFormMissionNumberInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormMissionNumberInput] input').type(textOver255);
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormMissionNumberInput] input').clear();
      cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightName);

      cy.get('[data-cy=FlightFormCotamNameInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormCotamNameInput] input').type(textOver255);
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormCotamNameInput] input').clear();
      cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightName);

      cy.get('[data-cy=FlightFormLegUserInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('not.be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('not.be.disabled');
      cy.get('[data-cy=FlightFormLegUserInput] input').type(textOver255);
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormLegUserInput] input').clear();
      cy.get('[data-cy=FlightFormLegUserInput] input').type(flightName);

      cy.get('[data-cy=LegFormDepartureInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();

      cy.get('[data-cy=LegFormArrivalInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();

      cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
      cy.get('.v-btn.text-grey').click();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      cy.intercept({
        method: 'POST',
        url: '/api/flight',
      }).as('flightCreate');
      cy.get('[data-cy=FlightFormOkBtn]').click();
      cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
      cy.url().should('match', /vols\/\d+\/edition/);
      cy.get('[data-cy=HeaderLogos]').click();

      cy.intercept({
        method: 'POST',
        url: '/api/flight/search*',
      }).as('flightSearch');
      cy.get('[data-cy=FlightListSearchInput] input').clear();
      cy.get('[data-cy=FlightListSearchInput] input').type(flightName);
      cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(flightName).should('exist');
    });

    it(`Avec plusieurs legs`, () => {
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');

      // remplissage du formulaire
      cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightLegsName);
      cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightLegsName);
      cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightLegsName);
      cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains(flightAircraft).click();
      cy.get('[data-cy=FlightFormLegUserInput] input').type(flightLegsName);

      // remplissage d'un leg
      cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
      cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
      cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      // 2ème leg
      cy.get('[data-cy=FlightFormAddLegBtn]').click();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=LegFormDepartureInput] input').eq(1).type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
      cy.get('[data-cy=LegFormArrivalInput] input').eq(1).type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
      cy.get('[data-cy=LegFormDepartureTimeInput] input').eq(1).parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      // 3ème leg
      cy.get('[data-cy=FlightFormAddLegBtn]').click();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=LegFormDepartureInput] input').eq(2).type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
      cy.get('[data-cy=LegFormArrivalInput] input').eq(2).type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
      cy.get('[data-cy=LegFormDepartureTimeInput] input').eq(2).parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      // 4ème leg
      cy.get('[data-cy=FlightFormAddLegBtn]').click();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=LegFormDepartureInput] input').eq(3).type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
      cy.get('[data-cy=LegFormArrivalInput] input').eq(3).type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
      cy.get('[data-cy=LegFormDepartureTimeInput] input').eq(3).parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      // 5ème leg
      cy.get('[data-cy=FlightFormAddLegBtn]').click();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=LegFormDepartureInput] input').eq(4).type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
      cy.get('[data-cy=LegFormArrivalInput] input').eq(4).type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
      cy.get('[data-cy=LegFormDepartureTimeInput] input').eq(4).parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');
      cy.get('[data-cy=LegFormDeleteBtn]').eq(4).click();
      cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

      cy.intercept({
        method: 'POST',
        url: '/api/flight',
      }).as('flightCreate');
      cy.get('[data-cy=FlightFormOkBtn]').click();
      cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
      cy.url().should('match', /vols\/\d+\/edition/);
      cy.get('[data-cy=HeaderLogos]').click();

      cy.intercept({
        method: 'POST',
        url: '/api/flight/search*',
      }).as('flightSearch');
      cy.get('[data-cy=FlightListSearchInput] input').clear();
      cy.get('[data-cy=FlightListSearchInput] input').type(flightLegsName);
      cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(flightLegsName).should('exist');
      const tableLine = cy.get('main .airvam-table tr:not(.disabled)').contains(flightLegsName).parent('tr');
      tableLine.contains('4').should('exist');
    });

    it(`En Free Seating`, () => {
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAddLegBtn]').should('be.disabled');

      // remplissage du formulaire
      cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightFSName);
      cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightFSName);
      cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightFSName);
      cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
      cy.get(".v-overlay-container .v-list .v-list-item").contains('Free Seating').click();
      cy.get('[data-cy=FlightFormLegUserInput] input').type(flightFSName);

      // remplissage d'un leg
      cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
      cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
      cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
      cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
      cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
      cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
      cy.get('.v-overlay-container .v-btn.text-green').click();

      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');

      cy.get('[data-cy=FlightFormAircraftNameInput] input').type(flightFSName);
      cy.get('[data-cy=FlightFormAircraftIdInput] input').type(flightFSName);

      cy.get('[data-cy=FlightFormOkBtn]').should('not.be.disabled');

      cy.get('[data-cy=FlightFormAircraftNameInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAircraftNameInput] input').type(textOver255);
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAircraftNameInput] input').clear();
      cy.get('[data-cy=FlightFormAircraftNameInput] input').type(flightFSName);


      cy.get('[data-cy=FlightFormAircraftIdInput] input').clear();
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAircraftIdInput] input').type(textOver255);
      cy.get('[data-cy=FlightFormOkBtn]').should('be.disabled');
      cy.get('[data-cy=FlightFormAircraftIdInput] input').clear();
      cy.get('[data-cy=FlightFormAircraftIdInput] input').type(flightFSName);

      cy.intercept({
        method: 'POST',
        url: '/api/flight',
      }).as('flightCreate');
      cy.get('[data-cy=FlightFormOkBtn]').click();
      cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
      cy.url().should('match', /vols\/\d+\/edition/);
      cy.get('[data-cy=HeaderLogos]').click();

      cy.intercept({
        method: 'POST',
        url: '/api/flight/search*',
      }).as('flightSearch');
      cy.get('[data-cy=FlightListSearchInput] input').clear();
      cy.get('[data-cy=FlightListSearchInput] input').type(flightFSName);
      cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
      cy.get('main .airvam-table tr:not(.disabled)').contains(flightFSName).should('exist');
    });
  });
});

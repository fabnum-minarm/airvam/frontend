describe('Parcours complet vol avec un plusieurs leg', () => {
  before(() => {
    cy.logout();
    cy.populateDb('user');
  });

  after(() => {
    cy.resetDb('flight');
  });

  const flightName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const flightAircraft = 'F-UJCU';
  const legDeparture1 = 'LFMI';
  const legArrival1 = 'LFPC';
  const legArrival2 = 'FMEE';
  const legDeparture3 = 'FMEE';
  const legArrival3 = 'SOCA';

  const paxName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const paxNameBis = `CYTEST_${Math.floor(Math.random() * 1000)}`;

  function goToFlightPage() {
    cy.devLogin(Cypress.env('devTestUser'));

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input', {timeout: 10000}).clear();
    cy.get('[data-cy=FlightListSearchInput] input').type(flightName);
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains(flightName).click();
    cy.url().should('match', /vols\/\d+\/edition/);
    cy.get('[data-cy=FlightTitleHeader]').contains(`Vol ${flightName} / ${legDeparture1} - ${legArrival3}`).should('exist');
  }

  it(`Création du vol`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/vols/nouveau/edition');

    // remplissage du formulaire
    cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightName);
    cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(flightAircraft).click();
    cy.get('[data-cy=FlightFormLegUserInput] input').type(flightName);

    // remplissage d'un leg
    cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture1);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture1).click();
    cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival1);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival1).click();
    cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
    cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
    cy.get('.v-overlay-container .v-btn.text-green').click();

    cy.intercept({
      method: 'POST',
      url: '/api/flight',
    }).as('flightCreate');
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
    cy.url().should('match', /vols\/\d+\/edition/);

    // Rajouts des autres legs
    cy.get('[data-cy=FlightFormAddLegBtn]').click();
    cy.get('[data-cy=LegFormArrivalInput] input').eq(1).type(legArrival2);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival2).click();
    cy.get('[data-cy=LegFormDepartureTimeInput] input').eq(1).parent().click();
    cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(16).click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(3).click();
    cy.get('.v-overlay-container .v-btn.text-green').click();

    cy.get('[data-cy=FlightFormAddLegBtn]').click();
    cy.get('[data-cy=LegFormArrivalInput] input').eq(2).type(legArrival3);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival3).click();
    cy.get('[data-cy=LegFormDepartureTimeInput] input').eq(2).parent().click();
    cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(18).click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(4).click();
    cy.get('.v-overlay-container .v-btn.text-green').click();

    cy.intercept({
      method: 'PATCH',
      url: '/api/flight/**',
    }).as('flightEdit');
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.wait('@flightEdit').its('response.statusCode').should('equal', 200);
  });

  it(`Ajout des PAX`, () => {
    goToFlightPage();

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);

    // cy.get('[data-cy=PaxDownloadFileBtn]').click();
    // cy.verifyDownload('Modele_fichier_PAX.xlsx');

    cy.intercept({
      method: 'POST',
      url: '/api/pax/parse/**',
    }).as('paxParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/PAX_TEST_MULTIPLE_LEGS.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@paxParse').its('response.statusCode').should('equal', 201);

    cy.get('[data-cy=PaxListErrorsFilterBtn]').parent().click();
    cy.get('[data-cy=PaxDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

    cy.get('.airvam-table tbody').find('tr').should('have.length', 285);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201)
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 285);
  });

  it(`Ajout des Animaux`, () => {
    goToFlightPage();

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseAnimalListBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/animal/);

    cy.get('.airvam-table').contains('La liste des animaux est vide').should('exist');
    cy.get('[data-cy=AnimalListErrorsFilterBtn]').should('not.exist');
    cy.get('[data-cy=AnimalListWarningsFilterBtn]').should('not.exist');
    cy.get('[data-cy=AnimalListDeleteBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalImportSaveBtn]').should('be.disabled');

    cy.intercept({
      method: 'POST',
      url: '/api/animal/parse/**',
    }).as('animalParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/ANIMAL_TEST_MULTIPLE_LEGS.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@animalParse').its('response.statusCode').should('equal', 201);

    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);
    cy.intercept({
      method: 'POST',
      url: '/api/animal/updateList/**',
    }).as('animalSave');
    cy.get('[data-cy=AnimalImportSaveBtn]').click();
    cy.wait('@animalSave').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=AnimalImportSaveBtn]').should('be.disabled');
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);
  });

  it(`Génération des plan cabines`, () => {
    goToFlightPage();

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseSeatsBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/plan_cabine/);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Incomplet').should('exist');

    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('be.disabled');
    cy.get('.balancy-zones').contains(`TOTAL PAX: 278`).should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 0);

    // Déblocage de six sièges normalement dédiés à l'équipage
    cy.intercept({
      method: 'POST',
      url: '/api/seat/**/unlock',
    }).as('seatUnlock');

    cy.get('[data-cy=SeatsActionsBlockModeBtn]').click();
    cy.get('[data-cy=SeatsActionsBlockModeBtn]').should('have.class', 'active');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).click();
    cy.wait('@seatUnlock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).click();
    cy.wait('@seatUnlock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).click();
    cy.wait('@seatUnlock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).click();
    cy.wait('@seatUnlock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).click();
    cy.wait('@seatUnlock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).click();
    cy.wait('@seatUnlock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .row').eq(0).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).should('not.have.class', 'seat-cell--blocked');

    cy.get('[data-cy=SeatsActionsBlockModeBtn]').click();
    cy.get('[data-cy=SeatsActionsBlockModeBtn]').should('not.have.class', 'active');

    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOA] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOA]').type('46');
    cy.get('[data-cy=BalancyFormInputZoneOB] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOB]').type('144');
    cy.get('[data-cy=BalancyFormInputZoneOC] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOC]').type('88');

    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/autoPlacement',
    }).as('legAutoPlacement');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);

    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 278);

    // LEG 2
    cy.get('[data-cy=DossierChooseLegBtn]').click();
    cy.get('[data-cy=DossierSelectLeg]').eq(1).click();
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Incomplet').should('exist');

    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('be.disabled');
    // cy.get('.balancy-zones').contains(`TOTAL PAX: 278`).should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);

    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    // cy.get('[data-cy=SeatCellPax]').should('have.length', 278);

    // LEG 3
    cy.get('[data-cy=DossierChooseLegBtn]').click();
    cy.get('[data-cy=DossierSelectLeg]').eq(2).click();
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Incomplet').should('exist');

    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('be.disabled');
    // cy.get('.balancy-zones').contains(`TOTAL PAX: 278`).should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);

    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    // cy.get('[data-cy=SeatCellPax]').should('have.length', 278);
  });

  it(`Finalisation des legs`, () => {
    goToFlightPage();
    cy.intercept({
      method: 'GET',
      url: '/api/seat/**',
    }).as('seatGet');

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseSeatsBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/plan_cabine/);

    // LEG 1
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('not.be.disabled');

    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/finalize',
    }).as('legFinalize');
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');

    // LEG 2
    cy.get('[data-cy=DossierChooseLegBtn]').click();
    cy.get('[data-cy=DossierSelectLeg]').eq(1).click();
    cy.wait('@seatGet').its('response.statusCode').should('equal', 200);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');

    // LEG 3 en N&B
    cy.get('[data-cy=DossierChooseLegBtn]').click();
    cy.get('[data-cy=DossierSelectLeg]').eq(2).click();
    cy.wait('@seatGet').its('response.statusCode').should('equal', 200);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').click();
    cy.get('[data-cy=FinalizeDialogRadioBlackBtn]').parent().click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');
  });

  it(`Vérification des documents générés`, () => {
    goToFlightPage();

    cy.get('[data-cy=FlightDocumentsOpenBtn]').click();
    // vol + 3 legs
    cy.get('.docs-wrapper').should('have.length', 4);
    // Tous les documents doivent avoir été générés
    cy.get('[data-cy=FlightDocumentsListDownloadBtn]').should('have.length', 28);
  });

  it(`Modification mineures après finalisation`, () => {
    goToFlightPage();
    cy.intercept({
      method: 'GET',
      url: '/api/seat/**',
    }).as('seatGet');

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);

    // Ajout d'un PAX et modification d'un PAX sur un leg en particulier
    cy.get('[data-cy=AddPaxBtn]').click();
    cy.get('[data-cy=PaxFormDepartureInput] input').type(legDeparture3);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture3).click();
    cy.get('[data-cy=PaxFormArrivalInput] input').type(legArrival3);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival3).click();
    cy.get('[data-cy=PaxFormWeightInput] input').type('90');
    cy.get('[data-cy=PaxFormLastnameInput] input').type(paxName);
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(paxName);
    cy.get('[data-cy=PaxFormRankInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains('OF-10').click();
    cy.get('[data-cy=PaxFormUnitInput] input').type('FABNUM');
    cy.get('[data-cy=PaxFormNationalityInput] input').type('FRA');

    cy.get('[data-cy=PaxFormOkBtn]').click();
    cy.get('main .airvam-table tr').contains(paxName).should('exist');

    cy.get('[data-cy=PaxListSearchInput] input').type('ALEKSANDARTOEDIT');
    cy.get('[data-cy=PaxEditBtn]').click();

    cy.get('[data-cy=PaxFormLastnameInput] input').clear();
    cy.get('[data-cy=PaxFormFirstnameInput] input').clear();
    cy.get('[data-cy=PaxFormNbrInput] input').clear();
    cy.get('[data-cy=PaxFormLastnameInput] input').type(paxNameBis);
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(paxNameBis);

    cy.get('[data-cy=PaxFormOkBtn]').click();
    cy.get('[data-cy=PaxListSearchInput] input').clear();

    cy.get('main .airvam-table tr').contains(paxNameBis).should('exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 286);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201);
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 286);
    cy.get('main .airvam-table tr').contains(paxName).should('exist');
    cy.get('main .airvam-table tr').contains(paxNameBis).should('exist');

    // Le leg passe en LMC
    // Génération des documents LMC

    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/autoPlacement',
    }).as('legAutoPlacement');
    cy.intercept({
      method: 'POST',
      url: '/api/seat/**/place/**',
    }).as('seatPlace');

    cy.get('[data-cy=FlightCaseSeatsBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/plan_cabine/);
    cy.get('[data-cy=DossierChooseLegBtn]').click();
    cy.get('[data-cy=DossierSelectLeg]').eq(2).click();
    cy.wait('@seatGet').its('response.statusCode').should('equal', 200);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('LMC').should('exist');
    // Aucune idée de pourquoi cypress refresh pas les données alors que ça roule nickel sur navigateur
    cy.reload();
    // On place le nouveau PAX à la main
    cy.get('[data-cy=SeatsActionsPlaceModeBtn]').click();
    cy.get('.seats-map > .row').eq(1).find('.seats-row').eq(17).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).click();
    cy.get('[data-cy=SeatPlaceSelectPaxCheckbox]').first().parent().click();
    cy.get('[data-cy=SeatPlacePlacePaxBtn]').click();
    cy.wait('@seatPlace').its('response.statusCode').should('equal', 201);

    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/finalize',
    }).as('legFinalize');
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');

    cy.get('[data-cy=FlightDocumentsOpenBtn]').click();
    // vol + 3 legs
    cy.get('.docs-wrapper').should('have.length', 4);
    // Tous les documents doivent avoir été générés
    cy.get('[data-cy=FlightDocumentsListDownloadBtn]').should('have.length', 33);
    // On force le click car il y a souvent un toast par dessus
    cy.get('[data-cy=FlightDocumentsCloseBtn]').click({force: true});

    // Suppression d'un PAX présent sur un leg en particulier

    cy.get('[data-cy=FlightCasePaxListBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=PaxListSearchInput] input').type('JEANTOEDIT');
    cy.get('[data-cy=PaxDeleteBtn]').click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=PaxListSearchInput] input').clear();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 285);
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201);
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 285);

    // Le leg passe en LMC
    // Génération des documents LMC
    cy.get('[data-cy=FlightCaseSeatsBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/plan_cabine/);
    cy.get('[data-cy=DossierChooseLegBtn]').click();
    cy.get('[data-cy=DossierSelectLeg]').eq(0).click();
    cy.get('[data-cy=FlightCaseLegStatus]').contains('LMC').should('exist');
    // Aucune idée de pourquoi cypress refresh pas les données alors que ça roule nickel sur navigateur
    cy.reload();
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]', {timeout: 10000}).click();
    cy.get('[data-cy=FinalizeDialogRadioFullBtn]', {timeout: 10000}).parent().click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');

    cy.get('[data-cy=FlightDocumentsOpenBtn]').click();
    // vol + 3 legs
    cy.get('.docs-wrapper').should('have.length', 4);
    // Tous les documents doivent avoir été générés
    cy.get('[data-cy=FlightDocumentsListDownloadBtn]').should('have.length', 33);
    // On force le click car il y a souvent un toast par dessus
    cy.get('[data-cy=FlightDocumentsCloseBtn]').click({force: true});

    // Modification d'un plan cabine sur un leg en particulier
    cy.get('[data-cy=DossierChooseLegBtn]').click();
    cy.get('[data-cy=DossierSelectLeg]').eq(1).click();
    cy.wait('@seatGet').its('response.statusCode').should('equal', 200);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');
    // Aucune idée de pourquoi cypress refresh pas les données alors que ça roule nickel sur navigateur
    cy.reload();
    cy.get('[data-cy=SeatsBalancingRadioExternalBtn]', {timeout: 10000}).parent().click();
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('LMC').should('exist');

    // Le leg passe en LMC
    // Génération des documents LMC
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');

    cy.get('[data-cy=FlightDocumentsOpenBtn]').click();
    // vol + 3 legs
    cy.get('.docs-wrapper').should('have.length', 4);
    // Tous les documents doivent avoir été générés
    cy.get('[data-cy=FlightDocumentsListDownloadBtn]').should('have.length', 39);
    // On force le click car il y a souvent un toast par dessus
    cy.get('[data-cy=FlightDocumentsCloseBtn]').click({force: true});
  });

  it(`Téléchargement des documents`, () => {
    goToFlightPage();
    cy.get('[data-cy=FlightDocumentsOpenBtn]').click();
    // Téléchargement du manifeste PAX et manifeste PAX LMC
    cy.get('.docs-wrapper').eq(3).contains('manifeste_pax.pdf').parent().find('[data-cy=FlightDocumentsListDownloadBtn]').click();
    cy.get('.docs-wrapper').eq(3).contains('manifeste_pax_lmc.pdf').parent().find('[data-cy=FlightDocumentsListDownloadBtn]').click();

    // Téléchargement de tous les documents d'un leg ou du vol
    cy.get('[data-cy=FlightDocumentsDownloadLegBtn]').eq(0).click();
    cy.get('[data-cy=FlightDocumentsDownloadFlightBtn]').click();
    cy.verifyDownload('manifeste_pax.pdf');
    cy.verifyDownload('manifeste_pax_lmc.pdf');
    cy.verifyDownload('.zip', { contains: true });
  });

  it(`Modification majeures après finalisation`, () => {
    goToFlightPage();

    // Modification de l'avion
    cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains('F-UJCS').click();

    cy.intercept({
      method: 'PATCH',
      url: '/api/flight/**',
    }).as('flightEdit');
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.wait('@flightEdit').its('response.statusCode').should('equal', 200);

    cy.get('[data-cy=FlightDocumentsOpenBtn]').should('not.exist');
  });
});

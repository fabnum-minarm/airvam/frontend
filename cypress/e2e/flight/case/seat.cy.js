describe('Test de la page Plan Cabine', () => {
  before(() => {
    cy.logout();
    cy.populateDb('user');
  });

  after(() => {
    cy.resetDb('flight');
  });

  const textOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
  const textOver50 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
  const flightName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const flightAircraft = 'F-UJCT';
  const legDeparture = 'LFMI';
  const legArrival = 'LFPC';

  it(`Création d'un vol`, () => {
    cy.devLogin(Cypress.env('devTestGod'));
    cy.visit('/vols/nouveau/edition');

    // remplissage du formulaire
    cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightName);
    cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(flightAircraft).click();
    cy.get('[data-cy=FlightFormLegUserInput] input').type(flightName);

    // remplissage d'un leg
    cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
    cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
    cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
    cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
    cy.get('.v-overlay-container .v-btn.text-green').click();

    cy.intercept({
      method: 'POST',
      url: '/api/flight',
    }).as('flightCreate');
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
    cy.url().should('match', /vols\/\d+\/edition/);

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.intercept({
      method: 'POST',
      url: '/api/pax/parse/**',
    }).as('paxParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/PAX_TEST_SINGLE_LEG_LFMI_LFPC_CLEAN.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@paxParse').its('response.statusCode').should('equal', 201);
    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201);
    cy.wait(1000);
    cy.get('[data-cy=FlightCaseAnimalListBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/animal/);
    cy.intercept({
      method: 'POST',
      url: '/api/animal/parse/**',
    }).as('animalParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/ANIMAL_TEST_SINGLE_LEG_LFMI_LFPC_CLEAN.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@animalParse').its('response.statusCode').should('equal', 201);
    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);

    cy.intercept({
      method: 'POST',
      url: '/api/animal/updateList/**',
    }).as('animalSave');
    cy.get('[data-cy=AnimalImportSaveBtn]').click();
    cy.wait('@animalSave').its('response.statusCode').should('equal', 201);
  });

  function goToFlightPage(login) {
    if(login) {
      cy.devLogin(Cypress.env('devTestGod'));
    } else {
      cy.visit('/vols', { timeout: 10000 });
    }

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input', { timeout: 10000 }).clear();
    cy.get('[data-cy=FlightListSearchInput] input').type(flightName);
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains(flightName).click();
    cy.url().should('match', /vols\/\d+\/edition/);
    cy.get('[data-cy=FlightTitleHeader]').contains(`Vol ${flightName} / ${legDeparture} - ${legArrival}`).should('exist');
  }

  function goToSeatPage() {
    goToFlightPage(true);

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseSeatsBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/plan_cabine/);
  }

  function editAircraftAndGoToSeatPage(aircraftName, login) {
    goToFlightPage(login);

    cy.intercept({
      method: 'PATCH',
      url: '/api/flight/**',
    }).as('flightEdit');

    cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(aircraftName).click();
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.wait('@flightEdit').its('response.statusCode').should('equal', 200);

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseSeatsBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/plan_cabine/);
  }

  it(`Au chargement de la page, un centrage doit être proposé par défaut`, () => {
    goToSeatPage();

    cy.get('[data-cy=FlightCaseLegStatus]').contains('Incomplet').should('exist');

    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('be.disabled');
    cy.get('.balancy-zones').contains(`TOTAL PAX: 17`).should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('[data-cy=BalancyFormInputZoneOA] input').should('have.value', '1');
    cy.get('[data-cy=BalancyFormInputZoneOB] input').should('have.value', '1');
    cy.get('[data-cy=BalancyFormInputZoneOC] input').should('have.value', '7');
    cy.get('[data-cy=BalancyFormInputZoneOD] input').should('have.value', '8');
  });

  it(`On ne doit pas pouvoir faire un placement auto tant que le centrage n'est pas ok`, () => {
    goToSeatPage();

    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('not.be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOA] input').clear();
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOA] input').type('2');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOA] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOA] input').type('1');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('not.be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOB] input').clear();
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOB]').type('2');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOB] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOB]').type('1');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('not.be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOC] input').clear();
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOC]').type('5');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOC] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOC]').type('7');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('not.be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOD] input').clear();
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOD]').type('10');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
    cy.get('[data-cy=BalancyFormInputZoneOD] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOD]').type('8');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('not.be.disabled');

    // Centrage ok mais zone A n'a que 16 places
    cy.get('[data-cy=BalancyFormInputZoneOA] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOA] input').type('17');
    cy.get('[data-cy=BalancyFormInputZoneOB] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOB] input').type('0');
    cy.get('[data-cy=BalancyFormInputZoneOC] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOC] input').type('0');
    cy.get('[data-cy=BalancyFormInputZoneOD] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOD] input').type('0');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').should('be.disabled');
  });

  it(`Pour chaque type d'avion, le plan cabine doit s'afficher correctement`, () => {
    editAircraftAndGoToSeatPage('F-UJCU', true);
    cy.get('[data-cy=BalancyFormInputZoneOA]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOA]').parents('.balancy-zone').contains('/40').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').parents('.balancy-zone').contains('/144').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').parents('.balancy-zone').contains('/88').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOD]').should('not.exist');
    editAircraftAndGoToSeatPage('F-UJCS', false);
    cy.get('[data-cy=BalancyFormInputZoneOA]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOA]').parents('.balancy-zone').contains('/12').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').parents('.balancy-zone').contains('/148').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').parents('.balancy-zone').contains('/110').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOD]').should('not.exist');
    editAircraftAndGoToSeatPage('88Y', false);
    cy.get('[data-cy=BalancyFormInputZoneOA]').should('not.exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').should('not.exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').parents('.balancy-zone').contains('/88').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOD]').should('not.exist');
    editAircraftAndGoToSeatPage('40-144-88', false);
    cy.get('[data-cy=BalancyFormInputZoneOA]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOA]').parents('.balancy-zone').contains('/32').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').parents('.balancy-zone').contains('/144').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').parents('.balancy-zone').contains('/88').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOD]').should('not.exist');
    editAircraftAndGoToSeatPage('F-UJCT', false);
    cy.get('[data-cy=BalancyFormInputZoneOA]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOA]').parents('.balancy-zone').contains('/16').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOB]').parents('.balancy-zone').contains('/16').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOC]').parents('.balancy-zone').contains('/96').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOD]').should('exist');
    cy.get('[data-cy=BalancyFormInputZoneOD]').parents('.balancy-zone').contains('/98').should('exist');
  });

  it(`Trois types de placement automatiques sont proposés`, () => {
    goToSeatPage();

    cy.get('[data-cy=SeatsBalancingRadioRowBtn]').should('exist');
    cy.get('[data-cy=SeatsBalancingRadioExternalBtn]').should('exist');
    cy.get('[data-cy=SeatsBalancingRadioOneOnTwoBtn]').should('exist');

    cy.get('[data-cy=SeatsBalancingRadioRowBtn]').parent().click();
    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/autoPlacement',
    }).as('legAutoPlacement');
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');

    // Vérification que les PAX sont bien placés
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 17);
    cy.get('.seats-map').contains('OA - 1').should('exist');
    cy.get('.seats-map > .v-row').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map').contains('OB - 1').should('exist');
    cy.get('.seats-map > .v-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(1).find('.seats-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(1).find('.seats-row').eq(1).find('.seat-cell').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map').contains('OC - 7').should('exist');
    cy.get('.seats-map > .v-row').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 7);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 7);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map').contains('OD - 8').should('exist');
    cy.get('.seats-map > .v-row').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 8);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 4);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 4);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('not.be.disabled');

    cy.get('[data-cy=BalancyFormInputZoneOA] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOA] input').type('0');
    cy.get('[data-cy=BalancyFormInputZoneOB] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOB] input').type('0');
    cy.get('[data-cy=BalancyFormInputZoneOC] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOC] input').type('17');
    cy.get('[data-cy=BalancyFormInputZoneOD] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOD] input').type('0');
    cy.get('[data-cy=SeatsBalancingRadioOneOnTwoBtn]').parent().click();
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');

    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 17);
    cy.get('.seats-map').contains('OA - 0').should('exist');
    cy.get('.seats-map > .v-row').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map').contains('OB - 0').should('exist');
    cy.get('.seats-map > .v-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map').contains('OC - 17').should('exist');
    cy.get('.seats-map > .v-row').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 17);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 4);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 4);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 4);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 4);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(5).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(5).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map').contains('OD - 0').should('exist');
    cy.get('.seats-map > .v-row').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('not.be.disabled');

    cy.get('[data-cy=BalancyFormInputZoneOA] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOA] input').type('2');
    cy.get('[data-cy=BalancyFormInputZoneOB] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOB] input').type('0');
    cy.get('[data-cy=BalancyFormInputZoneOC] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOC] input').type('4');
    cy.get('[data-cy=BalancyFormInputZoneOD] input').clear();
    cy.get('[data-cy=BalancyFormInputZoneOD] input').type('11');
    cy.get('[data-cy=SeatsBalancingRadioExternalBtn]').parent().click();
    cy.get('[data-cy=SeatsBalancingAutoPlacementBtn]').click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.wait('@legAutoPlacement').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');

    cy.get('[data-cy=FlightCaseLegStatus]').contains('Préparé').should('exist');
    cy.get('[data-cy=SeatCellPax]').should('have.length', 17);
    cy.get('.seats-map').contains('OA - 1').should('exist'); // CHECK: OA - 1 ? // CHECK: OA - 2 ?
    cy.get('.seats-map > .v-row').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 2);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 2);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map').contains('OB - 0').should('exist');
    cy.get('.seats-map > .v-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map').contains('OC - 4').should('exist');
    cy.get('.seats-map > .v-row').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 4);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map').contains('OD - 11').should('exist');
    cy.get('.seats-map > .v-row').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 11);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(2).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(4).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(5).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(6).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(4).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(7).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(5).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(5).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(3).find('.seats-row').eq(5).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('not.be.disabled');
  });

  it(`On doit pouvoir bloquer / débloquer des sièges`, () => {
    goToSeatPage();

    let interceptLock = false;
    cy.intercept({
      method: 'POST',
      url: '/api/seat/**/lock',
    }, (req) => {
      interceptLock = true;
      req.continue((res) => {
      });
    }).as('seatLock');

    cy.intercept({
      method: 'POST',
      url: '/api/seat/**/unlock',
    }).as('seatUnlock');

    cy.get('[data-cy=SeatCellPax]').should('have.length', 17);
    cy.get('.seats-map').contains('OA - 1').should('exist'); // CHECK: OA - 2 ?

    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 2);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);

    // Si on clique sur un siège avec aucune option de sélectionnée, cela ne fait rien
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).click();
    cy.wrap(interceptLock).should('eq', false);

    // Blocage d'un siège vide
    cy.get('[data-cy=SeatsActionsBlockModeBtn]').click();
    cy.get('[data-cy=SeatsActionsBlockModeBtn]').should('have.class', 'active');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).click();
    cy.wait('@seatLock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).should('have.class', 'seat-cell--blocked');

    // Blocage d'un siège avec PAX
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).click();
    cy.wait('@seatLock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    // Le pax doit être replacé sur le siège libre le plus proche
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);

    // Déblocage d'un siège
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).click();
    cy.wait('@seatUnlock').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).should('not.have.class', 'seat-cell--blocked');

    cy.get('[data-cy=SeatsActionsBlockModeBtn]').click();
    cy.get('[data-cy=SeatsActionsBlockModeBtn]').should('not.have.class', 'active');
  });

  it(`On doit pouvoir déplacer des PAX`, () => {
    goToSeatPage();

    let interceptPlace = false;
    cy.intercept({
      method: 'POST',
      url: '/api/seat/**/place/**',
    }, (req) => {
      interceptPlace = true;
      req.continue((res) => {
      });
    }).as('seatPlace');
    const dataTransfer = new DataTransfer();

    cy.get('[data-cy=SeatsActionsPlaceModeBtn]').click();
    cy.get('[data-cy=SeatsActionsPlaceModeBtn]').should('have.class', 'active');
    // On ne doit pas pouvoir drag and drop sur un siège bloqué
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).trigger('dragstart', {dataTransfer});
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).trigger('drop', {dataTransfer});
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).trigger('dragend', {dataTransfer});
    cy.wrap(interceptPlace).should('eq', false);

    // Déplacement d'un PAX sur un siège vide, déplacement d'un PAX de la zone C vers la zone A
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).click();
    cy.get('[data-cy=SeatPlaceMovePaxBtn]').click();
    cy.get('.Vue-Toastification__toast').should('exist');
    // On ne doit pas pouvoir déplacer un PAX sur un siège bloqué que ce soit par clic
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).click();
    cy.wrap(interceptPlace).should('eq', false);

    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).click();
    cy.wait('@seatPlace').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);

    // Déplacement d'un PAX sur un siège occupé avec drag & drop
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).trigger('dragstart', {dataTransfer});

    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).trigger('drop', {dataTransfer});
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).trigger('dragend', {dataTransfer});
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.wait('@seatPlace').its('response.statusCode').should('equal', 201);
    cy.get('.seats-map > .v-row').eq(2).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(2).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(3).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);

    cy.get('[data-cy=SeatsActionsPlaceModeBtn]').click();
    cy.get('[data-cy=SeatsActionsPlaceModeBtn]').should('not.have.class', 'active');
  });

  it(`On doit pouvoir commenter des sièges`, () => {
    goToSeatPage();

    cy.intercept({
      method: 'POST',
      url: '/api/seat/**/comment',
    }).as('seatComment');

    cy.get('[data-cy=SeatsActionsCommentModeBtn]').click();
    cy.get('[data-cy=SeatsActionsCommentModeBtn]').should('have.class', 'active');

    // Commentaire sur plusieurs sièges (avec PAX, bloqué et vide)
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).find('[data-cy=SeatCellPax]').should('have.length', 1);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).click();
    cy.get('.seat-comment-card').contains('Laisser un commentaire').should('exist');
    cy.get('[data-cy=SeatCommentObservationInput] textarea').first().type(textOver255);
    cy.get('[data-cy=SeatCommentOkBtn]').should('be.disabled');
    cy.get('[data-cy=SeatCommentObservationInput] textarea').first().clear();
    cy.get('[data-cy=SeatCommentObservationInput] textarea').first().type('Siège avec PAX');
    cy.get('[data-cy=SeatCommentOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=SeatCommentOkBtn]').click();
    cy.wait('@seatComment').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(0).should('have.class', 'seat-cell--comment');

    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).click();
    cy.get('[data-cy=SeatCommentObservationInput] textarea').last().type('Siège HS');
    cy.get('[data-cy=SeatCommentOkBtn]').last().click();
    cy.wait('@seatComment').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('have.class', 'seat-cell--comment');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(1).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('have.class', 'seat-cell--blocked');

    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('not.have.class', 'seat-cell--blocked');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).find('[data-cy=SeatCellPax]').should('have.length', 0);
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).click();
    cy.get('[data-cy=SeatCommentObservationInput] textarea').last().type('Siège vide');
    cy.get('[data-cy=SeatCommentOkBtn]').last().click();
    cy.wait('@seatComment').its('response.statusCode').should('equal', 201);
    cy.get('.Vue-Toastification__toast').should('exist');
    cy.get('.seats-map > .v-row').eq(0).find('.seats-row').eq(3).find('.seat-cell:not(.seat-cell--header, .seat-cell--hide)').eq(1).should('have.class', 'seat-cell--comment');

    cy.get('[data-cy=SeatsActionsCommentModeBtn]').click();
    cy.get('[data-cy=SeatsActionsCommentModeBtn]').should('not.have.class', 'active');
  });

  it(`La légende doit bien s'appliquer au plan cabine`, () => {
    goToSeatPage();

    // On doit pouvoir regarder la légende du plan cabine
    cy.get('[data-cy=SeatsActionsLegendBtn]').click();
    cy.get('.v-card').contains('Légende').should('exist');

    // Vérification des background de couleur par rapport aux grades
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OF-10.*alex', 'i')).parents('.seat-cell').should('have.class', 'bgGeneralOfficers');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OF-5.*pierre', 'i')).parents('.seat-cell').should('have.class', 'bgSeniorOfficers');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OF-2.*thomas', 'i')).parents('.seat-cell').should('have.class', 'bgJuniorOfficers');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OR-7.*jeremy', 'i')).parents('.seat-cell').should('have.class', 'bgSeniorSubOfficers');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OR-5.*ryan', 'i')).parents('.seat-cell').should('have.class', 'bgJuniorSubOfficers');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OR-2.*marvin', 'i')).parents('.seat-cell').should('have.class', 'bgTroopRanks');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('CIV.*hugo', 'i')).parents('.seat-cell').should('have.class', 'bgCivilRanks');

    // Vérification des icônes
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('BB.*thibault', 'i')).parents('.seat-cell').find('.v-icon').contains('child_care').should('exist');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OR-5.*quentin', 'i')).parents('.seat-cell').find('.v-icon').contains('star').should('exist');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OR-3.*alexandre', 'i')).parents('.seat-cell').find('.v-icon').contains('medical_services').should('exist');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OR-7.*mickael', 'i')).parents('.seat-cell').find('.v-icon').contains('work').should('exist');
    // cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OF-5.*pierre', 'i')).parents('.seat-cell').find('.v-icon').contains('pets').should('exist');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OR-1.*charly', 'i')).parents('.seat-cell').find('.v-icon').contains('pets').should('exist');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OF-1.*stephano', 'i')).parents('.seat-cell').find('.v-icon').contains('pets').should('exist');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OF-10.*alex', 'i')).parents('.seat-cell').find('.v-icon').contains('pets').should('exist');
    cy.get('[data-cy=SeatCellPax]').contains(new RegExp('OF-2.*thomas', 'i')).parents('.seat-cell').find('.v-icon').contains('pets').should('exist');
  });

  it(`On doit pouvoir se déplacer sur le plan cabine (zoom, fullscreen)`, () => {
    goToSeatPage();

    // Vérification du zoom / dézoom
    cy.get('[data-cy=SeatsActionsZoomInBtn]').click();
    cy.get('[data-cy=SeatsActionsZoomOutBtn]').click();

    // Vérification du passage en fullscreen
    cy.get('[data-cy=SeatsActionsFullscreenBtn]').click();
    cy.get('[data-cy=SeatsActionsFullscreenBtn]').click();
  });

  it(`Quand tout les PAX sont placés on doit pouvoir finaliser le leg`, () => {
    goToSeatPage();

    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').should('not.be.disabled');

    cy.intercept({
      method: 'POST',
      url: '/api/leg/**/finalize',
    }).as('legFinalize');
    cy.get('[data-cy=SeatsLayoutFinalizeBtn]').click();
    cy.get('[data-cy=FinalizeDialogConfirmBtn]').click();
    cy.wait('@legFinalize').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=FlightCaseLegStatus]').contains('Finalisé').should('exist');
    cy.get('[data-cy=FlightDocumentsOpenBtn]').click();
    cy.get('.docs-wrapper').should('have.length', 2);
    // Tout les documents doivent avoir été générés
    cy.get('[data-cy=FlightDocumentsListDownloadBtn]').should('have.length', 10);
  });
});
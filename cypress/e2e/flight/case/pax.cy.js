describe('Test de la page PAX', () => {
  before(() => {
    cy.logout();
    cy.populateDb('user');
  });

  after(() => {
    cy.resetDb('flight');
  });

  const textOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
  const textOver50 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
  const flightName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const flightAircraft = '88Y';
  const legDeparture = 'LFMI';
  const legArrival = 'LFPC';

  const paxName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const paxNameBis = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const paxRank = 'OF-10 - OTAN';
  const paxWeight = '90';
  const paxUnit = 'MINARM';
  const paxNationality = 'Française';

  it(`Création d'un vol`, () => {
    cy.devLogin(Cypress.env('devTestUser'));
    cy.visit('/vols/nouveau/edition');

    // remplissage du formulaire
    cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightName);
    cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(flightAircraft).click();
    cy.get('[data-cy=FlightFormLegUserInput] input').type(flightName);

    // remplissage d'un leg
    cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
    cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
    cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
    cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
    cy.get('.v-overlay-container .v-btn.text-green').click();

    cy.intercept({
      method: 'POST',
      url: '/api/flight',
    }).as('flightCreate');
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
    cy.url().should('match', /vols\/\d+\/edition/);
  });

  function goToPaxPage() {
    cy.devLogin(Cypress.env('devTestUser'));

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type(flightName);
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains(flightName).click();
    cy.url().should('match', /vols\/\d+\/edition/);
    cy.get('[data-cy=FlightTitleHeader]').contains(`Vol ${flightName} / ${legDeparture} - ${legArrival}`).should('exist');

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
  }

  it(`Ajout des PAX via fichier`, () => {
    goToPaxPage();

    // cy.get('[data-cy=PaxDownloadFileBtn]').click();
    // cy.verifyDownload('Modele_fichier_PAX.xlsx');

    cy.get('.airvam-table').contains('La liste des PAX est vide').should('exist');
    cy.get('[data-cy=PaxListErrorsFilterBtn]').should('not.exist');
    cy.get('[data-cy=PaxListWarningsFilterBtn]').should('not.exist');
    cy.get('[data-cy=PaxListDeleteBtn]').should('be.disabled');
    cy.get('[data-cy=PaxImportSaveBtn]').should('be.disabled');

    cy.intercept({
      method: 'POST',
      url: '/api/pax/parse/**',
    }).as('paxParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/PAX_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@paxParse').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=PaxListCountBadge]').should('contain', '20');
    cy.get('[data-cy=PaxListErrorsBadge]').should('contain', '2');
    cy.get('[data-cy=PaxListWarningsBadge]').should('contain', '2');
    cy.get('.airvam-table tbody').find('tr').should('have.length', 20);

    cy.get('[data-cy=PaxListErrorsFilterBtn]').parent().click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 2);
    cy.get('.airvam-table thead .v-input--selection-controls__input').click();
    cy.get('[data-cy=PaxListDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=PaxListErrorsBadge]').should('not.exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 18);

    cy.get('[data-cy=PaxListWarningsFilterBtn]').parent().click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 2);
    cy.get('[data-cy=PaxDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=PaxListWarningsBadge]').should('not.exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=PaxImportSaveBtn]').should('be.disabled');
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);

    // Reupload d'un fichier avec suppression
    cy.get('input[type=file]').selectFile('cypress/fixtures/PAX_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.get('.v-dialog').contains('Import d\'un nouveau fichier').should('exist');
    cy.get('[data-cy=ImportDialogCloseBtn]').click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);
    cy.get('input[type=file]').selectFile('cypress/fixtures/PAX_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.get('[data-cy=ImportDialogFormDeleteData]').parent().click();
    cy.get('[data-cy=ImportDialogOkBtn]').click();
    cy.wait('@paxParse').its('response.statusCode').should('equal', 201);
    cy.get('.airvam-table tbody').find('tr').should('have.length', 20);

    cy.get('[data-cy=PaxListErrorsFilterBtn]').parent().click();
    cy.get('.airvam-table thead .v-input--selection-controls__input').click();
    cy.get('[data-cy=PaxListDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

    cy.get('[data-cy=PaxListWarningsFilterBtn]').parent().click();
    cy.get('[data-cy=PaxDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201);
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);
  });

  it(`Ajout des PAX via le formulaire`, () => {
    goToPaxPage();

    cy.get('[data-cy=AddPaxBtn]').click();
    cy.get('.v-dialog').contains('Création d\'un PAX').should('exist');

    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    // remplissage des champs obligatoires du formulaire
    cy.get('[data-cy=PaxFormDepartureInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
    cy.get('[data-cy=PaxFormArrivalInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
    cy.get('[data-cy=PaxFormWeightInput] input').type(paxWeight);
    cy.get('[data-cy=PaxFormLastnameInput] input').type(paxName);
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(paxName);
    cy.get('[data-cy=PaxFormRankInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(paxRank).click();
    cy.get('[data-cy=PaxFormUnitInput] input').type(paxUnit);
    cy.get('[data-cy=PaxFormNationalityInput] input').type(paxNationality);
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');

    // Annuler
    cy.get('[data-cy=PaxFormCloseBtn]').click();
    cy.get('main .airvam-table tr').contains(paxName).should('not.exist');

    cy.get('[data-cy=AddPaxBtn]').click();
    cy.get('[data-cy=PaxFormDepartureInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
    cy.get('[data-cy=PaxFormArrivalInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
    cy.get('[data-cy=PaxFormWeightInput] input').type(paxWeight);
    cy.get('[data-cy=PaxFormLastnameInput] input').type(paxName);
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(paxName);
    cy.get('[data-cy=PaxFormRankInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(paxRank).click();
    cy.get('[data-cy=PaxFormUnitInput] input').type(paxUnit);
    cy.get('[data-cy=PaxFormNationalityInput] input').type(paxNationality);
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');

    cy.get('[data-cy=PaxFormAtrInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormAtrInput] input').clear();
    cy.get('[data-cy=PaxFormAtrInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormAtrInput] input').clear();

    cy.get('[data-cy=PaxFormDepartureInput] input').clear();
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormDepartureInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains('Aucun aéroport ne correspond à cette recherche').should('exist');
    cy.get('[data-cy=PaxFormDepartureInput] input').clear();
    cy.get('[data-cy=PaxFormDepartureInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();

    cy.get('[data-cy=PaxFormArrivalInput] input').clear();
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormArrivalInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains('Aucun aéroport ne correspond à cette recherche').should('exist');
    cy.get('[data-cy=PaxFormArrivalInput] input').clear();
    cy.get('[data-cy=PaxFormArrivalInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();

    cy.get('[data-cy=PaxFormNbrInput] input').type('A');
    cy.get('[data-cy=PaxFormNbrInput] input').contains('A').should('not.exist');
    cy.get('[data-cy=PaxFormNbrInput] input').clear();
    cy.get('[data-cy=PaxFormNbrInput] input').type('-1');
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled')
    cy.get('[data-cy=PaxFormNbrInput] input').clear();
    cy.get('[data-cy=PaxFormNbrInput] input').type('1001');
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormNbrInput] input').clear();
    cy.get('[data-cy=PaxFormNbrInput] input').type('2');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormNbrInput] input').clear();

    cy.get('[data-cy=PaxFormWeightInput] input').clear();
    cy.get('[data-cy=PaxFormWeightInput] input').type('A');
    cy.get('[data-cy=PaxFormWeightInput] input').contains('A').should('not.exist');
    cy.get('[data-cy=PaxFormWeightInput] input').clear();
    cy.get('[data-cy=PaxFormWeightInput] input').type('-1');
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled')
    cy.get('[data-cy=PaxFormWeightInput] input').clear();
    cy.get('[data-cy=PaxFormWeightInput] input').type('1001');
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormWeightInput] input').clear();
    cy.get('[data-cy=PaxFormWeightInput] input').type('2');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormWeightInput] input').clear();
    cy.get('[data-cy=PaxFormWeightInput] input').type(paxWeight);

    cy.get('[data-cy=PaxFormLuggageWeightInput] input').type('A');
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').contains('A').should('not.exist');
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').clear();
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').type('-1');
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled')
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').clear();
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').type('10001');
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').clear();
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').type('1000');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormLuggageWeightInput] input').clear();

    cy.get('[data-cy=PaxFormGenderInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormGenderInput] input').clear();
    cy.get('[data-cy=PaxFormGenderInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormGenderInput] input').clear();

    cy.get('[data-cy=PaxFormLastnameInput] input').clear();
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormLastnameInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormLastnameInput] input').clear();
    cy.get('[data-cy=PaxFormLastnameInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormLastnameInput] input').clear();
    cy.get('[data-cy=PaxFormLastnameInput] input').type(paxName);

    cy.get('[data-cy=PaxFormFirstnameInput] input').clear();
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormFirstnameInput] input').clear();
    cy.get('[data-cy=PaxFormFirstnameInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormFirstnameInput] input').clear();
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(paxName);

    cy.get('[data-cy=PaxFormRankInput] input').clear();
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormRankInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(paxRank).click();

    cy.get('[data-cy=PaxFormCategoryInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormCategoryInput] input').clear();
    cy.get('[data-cy=PaxFormCategoryInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormCategoryInput] input').clear();

    cy.get('[data-cy=PaxFormMilitaryIdInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormMilitaryIdInput] input').clear();
    cy.get('[data-cy=PaxFormMilitaryIdInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormMilitaryIdInput] input').clear();

    cy.get('[data-cy=PaxFormBirthdateInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormBirthdateInput] input').clear();
    cy.get('[data-cy=PaxFormBirthdateInput] input').type('01/01/2022');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormBirthdateInput] input').clear();

    cy.get('[data-cy=PaxFormUnitInput] input').clear();
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormUnitInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormUnitInput] input').clear();
    cy.get('[data-cy=PaxFormUnitInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormUnitInput] input').clear();
    cy.get('[data-cy=PaxFormUnitInput] input').type(paxUnit);

    cy.get('[data-cy=PaxFormNationalityInput] input').clear();
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormNationalityInput] input').type(textOver50);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormNationalityInput] input').clear();
    cy.get('[data-cy=PaxFormNationalityInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormNationalityInput] input').clear();
    cy.get('[data-cy=PaxFormNationalityInput] input').type(paxUnit);

    cy.get('[data-cy=PaxFormObservationsInput] input').type(textOver255);
    cy.get('[data-cy=PaxFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=PaxFormObservationsInput] input').clear();
    cy.get('[data-cy=PaxFormObservationsInput] input').type('A');
    cy.get('[data-cy=PaxFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=PaxFormObservationsInput] input').clear();

    cy.get('[data-cy=PaxFormOkBtn]').click();
    cy.get('main .airvam-table tr').contains(paxName).should('exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 18);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201);
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 18);
  });

  it(`Edition d'un PAX via le formulaire`, () => {
    goToPaxPage();

    cy.get('[data-cy=PaxListSearchInput] input').type(paxName);
    cy.get('[data-cy=PaxEditBtn]').click();
    cy.get('.v-dialog').contains('Édition d\'un PAX').should('exist');

    cy.get('[data-cy=PaxFormLastnameInput] input').clear();
    cy.get('[data-cy=PaxFormFirstnameInput] input').clear();
    cy.get('[data-cy=PaxFormNbrInput] input').clear();
    cy.get('[data-cy=PaxFormLastnameInput] input').type(paxNameBis);
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(paxNameBis);

    cy.get('[data-cy=PaxFormOkBtn]').click();
    cy.get('[data-cy=PaxListSearchInput] input').clear();
    cy.get('main .airvam-table tr').contains(paxName).should('not.exist');
    cy.get('main .airvam-table tr').contains(paxNameBis).should('exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 18);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201)
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 18);
    cy.get('main .airvam-table tr').contains(paxName).should('not.exist');
    cy.get('main .airvam-table tr').contains(paxNameBis).should('exist');
  });

  it(`Si on fait des modifications et qu'on change de page, une popin doit apparaître pour nous prévenir`, () => {
    goToPaxPage();

    cy.get('[data-cy=PaxListSearchInput] input').type(paxNameBis);
    cy.get('[data-cy=PaxEditBtn]').click();
    cy.get('[data-cy=PaxFormLastnameInput] input').clear();
    cy.get('[data-cy=PaxFormFirstnameInput] input').clear();
    cy.get('[data-cy=PaxFormNbrInput] input').clear();
    cy.get('[data-cy=PaxFormLastnameInput] input').type(paxName);
    cy.get('[data-cy=PaxFormFirstnameInput] input').type(paxName);

    cy.get('[data-cy=PaxFormOkBtn]').click();
    cy.get('[data-cy=PaxListSearchInput] input').clear();

    cy.get('[data-cy=ReturnFlightHeaderBtn]').click();
    cy.get('.v-dialog').contains(`Êtes-vous sûr de vouloir quitter la page sans enregistrer vos travaux ?`).should('exist');
    cy.get('[data-cy=ConfirmDialogCancelBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);

    cy.get('[data-cy=ReturnFlightHeaderBtn]').click();
    cy.get('.v-dialog').contains(`Êtes-vous sûr de vouloir quitter la page sans enregistrer vos travaux ?`).should('exist');
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.url().should('match', /vols/);
  });

  it(`On doit pouvoir exporter la liste courante sous format excel`, () => {
    goToPaxPage();
    cy.get('[data-cy=PaxImportExportBtn]').click();
    cy.verifyDownload('LISTE_PAX.xlsx');
  });

  // it(`On doit pouvoir télécharger le modèle de fichier`, () => {
  //   goToPaxPage();
  //
  //   cy.get('[data-cy=PaxDownloadFileBtn]').click();
  //   cy.verifyDownload('Modele_fichier_PAX.xlsx');
  // });
});

describe('Test de la page Animaux', () => {
  before(() => {
    cy.logout();
    cy.populateDb('user');
  });

  after(() => {
    cy.resetDb('flight');
  });

  const textOver255 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
  const textOver50 = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
  const flightName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const flightAircraft = '88Y';
  const legDeparture = 'LFMI';
  const legArrival = 'LFPC';

  const animalName = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const animalNameBis = `CYTEST_${Math.floor(Math.random() * 1000)}`;
  const animalUnit = 'BA104';
  const ownerName = `CHARLY LILIANA`;
  const ownerRank = 'OR-1';
  const notOwnerRank = 'OR-3';

  it.only(`Création d'un vol`, () => {
    cy.devLogin(Cypress.env('devTestGod'));
    cy.visit('/vols/nouveau/edition');

    // remplissage du formulaire
    cy.get('[data-cy=FlightFormMissionNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormMissionNumberInput] input').type(flightName);
    cy.get('[data-cy=FlightFormCotamNameInput] input').type(flightName);
    cy.get('[data-cy=FlightFormAircraftInput] input').parent().click();
    cy.get(".v-overlay-container .v-list .v-list-item").contains(flightAircraft).click();
    cy.get('[data-cy=FlightFormLegUserInput] input').type(flightName);

    // remplissage d'un leg
    cy.get('[data-cy=LegFormDepartureInput] input').type(legDeparture);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legDeparture).click();
    cy.get('[data-cy=LegFormArrivalInput] input').type(legArrival);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(legArrival).click();
    cy.get('[data-cy=LegFormDepartureTimeInput] input').parent().click();
    cy.get('.v-overlay-container .v-date-picker .v-picker__body .v-date-picker-month__day--selected').click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(14).click();
    cy.get('.v-overlay-container .v-time-picker-clock .v-time-picker-clock__item').eq(2).click();
    cy.get('.v-overlay-container .v-btn.text-green').click();

    cy.intercept({
      method: 'POST',
      url: '/api/flight',
    }).as('flightCreate');
    cy.get('[data-cy=FlightFormOkBtn]').click();
    cy.wait('@flightCreate').its('response.statusCode').should('equal', 201);
    cy.url().should('match', /vols\/\d+\/edition/);

    cy.wait(1000).get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.intercept({
      method: 'POST',
      url: '/api/pax/parse/**',
    }).as('paxParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/PAX_TEST_SINGLE_LEG_LFMI_LFPC_CLEAN.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@paxParse').its('response.statusCode').should('equal', 201);
    cy.get('.airvam-table tbody').find('tr').should('have.length', 17);

    cy.intercept({
      method: 'POST',
      url: '/api/pax/updateList/**',
    }).as('paxSave');
    cy.get('[data-cy=PaxImportSaveBtn]').click();
    cy.wait('@paxSave').its('response.statusCode').should('equal', 201);
  });

  function goToAnimalPage() {
    cy.devLogin(Cypress.env('devTestUser')); // TODO: Test fails with devTestUser when trying to show the flight created

    cy.intercept({
      method: 'POST',
      url: '/api/flight/search*',
    }).as('flightSearch');
    cy.get('[data-cy=FlightListSearchInput] input').clear();
    cy.get('[data-cy=FlightListSearchInput] input').type(flightName);
    cy.wait('@flightSearch').its('response.statusCode').should('equal', 201);
    cy.get('main .airvam-table tr:not(.disabled)').contains(flightName).click();
    cy.url().should('match', /vols\/\d+\/edition/);
    cy.get('[data-cy=FlightTitleHeader]').contains(`Vol ${flightName} / ${legDeparture} - ${legArrival}`).should('exist');

    cy.get('[data-cy=FlightCaseHeaderBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/pax/);
    cy.get('[data-cy=FlightCaseAnimalListBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/animal/);
  }

  it(`Ajout des Animaux via fichier`, () => {
    goToAnimalPage();

    cy.get('.airvam-table').contains('La liste des animaux est vide').should('exist');
    cy.get('[data-cy=AnimalListErrorsFilterBtn]').should('not.exist');
    cy.get('[data-cy=AnimalListWarningsFilterBtn]').should('not.exist');
    cy.get('[data-cy=AnimalListDeleteBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalImportSaveBtn]').should('be.disabled');

    cy.intercept({
      method: 'POST',
      url: '/api/animal/parse/**',
    }).as('animalParse');
    cy.get('input[type=file]').selectFile('cypress/fixtures/ANIMAL_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.wait('@animalParse').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=AnimalListCountBadge]').should('contain', '7');
    cy.get('[data-cy=AnimalListErrorsBadge]').should('contain', '2');
    cy.get('[data-cy=AnimalListWarningsBadge]').should('contain', '2');
    cy.get('.airvam-table tbody').find('tr').should('have.length', 7);

    cy.get('[data-cy=AnimalListErrorsFilterBtn]').parent().click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 2);
    cy.get('.airvam-table thead .v-input--selection-controls__input').click();
    cy.get('[data-cy=AnimalListDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=AnimalListErrorsBadge]').should('not.exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);

    cy.get('[data-cy=AnimalListWarningsFilterBtn]').parent().click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 2);
    cy.get('[data-cy=AnimalDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.get('[data-cy=AnimalListWarningsBadge]').should('not.exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);

    cy.intercept({
      method: 'POST',
      url: '/api/animal/updateList/**',
    }).as('animalSave');
    cy.get('[data-cy=AnimalImportSaveBtn]').click();
    cy.wait('@animalSave').its('response.statusCode').should('equal', 201);
    cy.get('[data-cy=AnimalImportSaveBtn]').should('be.disabled');
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);

    // Reupload d'un fichier avec suppression
    cy.get('input[type=file]').selectFile('cypress/fixtures/ANIMAL_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.get('.v-dialog').contains('Import d\'un nouveau fichier').should('exist');
    cy.get('[data-cy=ImportDialogCloseBtn]').click();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);
    cy.get('input[type=file]').selectFile('cypress/fixtures/ANIMAL_TEST_SINGLE_LEG_LFMI_LFPC.xlsx', {
      action: 'drag-drop',
      force: true
    });
    cy.get('[data-cy=ImportDialogFormDeleteData]').parent().click();
    cy.get('[data-cy=ImportDialogOkBtn]').click();
    cy.wait('@animalParse').its('response.statusCode').should('equal', 201);
    cy.get('.airvam-table tbody').find('tr').should('have.length', 7);

    cy.get('[data-cy=AnimalListErrorsFilterBtn]').parent().click();
    cy.get('.airvam-table thead .v-input--selection-controls__input').click();
    cy.get('[data-cy=AnimalListDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

    cy.get('[data-cy=AnimalListWarningsFilterBtn]').parent().click();
    cy.get('[data-cy=AnimalDeleteBtn]').first().click();
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();

    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);
    cy.get('[data-cy=AnimalImportSaveBtn]').click();
    cy.wait('@animalSave').its('response.statusCode').should('equal', 201);
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 4);
  });

  it(`Ajout des Animaux via le formulaire`, () => {
    goToAnimalPage();

    cy.get('[data-cy=AddAnimalBtn]').click();
    cy.get('.v-dialog').contains('Création d\'un animal').should('exist');

    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    // remplissage des champs obligatoires du formulaire
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').type(animalName);
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').type(animalName);
    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').type(animalUnit);
    cy.get('[data-cy=AnimalFormOwnerNameInput] input').type(ownerName);
    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').type(animalUnit);
    cy.get('[data-cy=AnimalFormOwnerRankInput] input').type(ownerRank);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(ownerRank).click();
    cy.get('[data-cy=AnimalFormOkBtn]').should('not.be.disabled');

    // Annuler
    cy.get('[data-cy=AnimalFormCloseBtn]').click();
    cy.get('main .airvam-table tr').contains(animalName).should('not.exist');

    cy.get('[data-cy=AddAnimalBtn]').click();
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').type(animalName);
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').type(animalName);
    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').type(animalUnit);
    cy.get('[data-cy=AnimalFormOwnerNameInput] input').type(ownerName);
    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').type(animalUnit);
    cy.get('[data-cy=AnimalFormOwnerRankInput] input').type(ownerRank);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(ownerRank).click();
    cy.get('[data-cy=AnimalFormOkBtn]').should('not.be.disabled');

    cy.get('[data-cy=AnimalFormAnimalNameInput] input').clear();
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').type(textOver50);
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').type('A');
    cy.get('[data-cy=AnimalFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').type(animalName);

    cy.get('[data-cy=AnimalFormAnimalIdInput] input').clear();
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').type(textOver50);
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').type('A');
    cy.get('[data-cy=AnimalFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').type(animalName);

    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').clear();
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').type(textOver50);
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').type('A');
    cy.get('[data-cy=AnimalFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalUnitInput] input').type(animalUnit);

    cy.get('[data-cy=AnimalFormOwnerNameInput] input').clear();
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerNameInput] input').type(textOver255);
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerNameInput] input').clear();
    cy.get('[data-cy=AnimalFormOwnerNameInput] input').type('A');
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerNameInput] input').clear();
    cy.get('[data-cy=AnimalFormOwnerNameInput] input').type(ownerName);

    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').clear();
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').type(textOver50);
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').clear();
    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').type('A');
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').clear();
    cy.get('[data-cy=AnimalFormOwnerUnitInput] input').type(animalUnit);

    cy.get('[data-cy=AnimalFormOwnerRankInput] input').clear();
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerRankInput] input').type(notOwnerRank);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(notOwnerRank).click();
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerRankInput] input').clear();
    cy.get('[data-cy=AnimalFormOwnerRankInput] input').type(ownerRank);
    cy.get(".v-overlay-container .v-list .v-list-item").contains(ownerRank).click();

    cy.get('[data-cy=AnimalFormOwnerObservationsInput] input').type(textOver255);
    cy.get('[data-cy=AnimalFormOkBtn]').should('be.disabled');
    cy.get('[data-cy=AnimalFormOwnerObservationsInput] input').clear();
    cy.get('[data-cy=AnimalFormOwnerObservationsInput] input').type('A');
    cy.get('[data-cy=AnimalFormOkBtn]').should('not.be.disabled');
    cy.get('[data-cy=AnimalFormOwnerObservationsInput] input').clear();

    cy.get('[data-cy=AnimalFormOkBtn]').click();
    cy.get('main .airvam-table tr').contains(animalName).should('exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);

    cy.intercept({
      method: 'POST',
      url: '/api/animal/updateList/**',
    }).as('animalSave');
    cy.get('[data-cy=AnimalImportSaveBtn]').click();
    cy.wait('@animalSave').its('response.statusCode').should('equal', 201)
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);
  });

  it(`Edition d'un animal via le formulaire`, () => {
    goToAnimalPage();

    cy.get('[data-cy=AnimalListSearchInput] input').type(animalName);
    cy.get('[data-cy=AnimalEditBtn]').click();
    cy.get('.v-dialog').contains('Édition d\'un animal').should('exist');

    cy.get('[data-cy=AnimalFormAnimalNameInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').type(animalNameBis);
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').type(animalNameBis);

    cy.get('[data-cy=AnimalFormOkBtn]').click();
    cy.get('[data-cy=AnimalListSearchInput] input').clear();
    cy.get('main .airvam-table tr').contains(animalName).should('not.exist');
    cy.get('main .airvam-table tr').contains(animalNameBis).should('exist');

    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);

    cy.intercept({
      method: 'POST',
      url: '/api/animal/updateList/**',
    }).as('animalSave');
    cy.get('[data-cy=AnimalImportSaveBtn]').click();
    cy.wait('@animalSave').its('response.statusCode').should('equal', 201);
    cy.reload();
    cy.get('.airvam-table tbody').find('tr').should('have.length', 5);
    cy.get('main .airvam-table tr').contains(animalName).should('not.exist');
    cy.get('main .airvam-table tr').contains(animalNameBis).should('exist');
  });

  it(`Si on fait des modifications et qu'on change de page, une popin doit apparaître pour nous prévenir`, () => {
    goToAnimalPage();

    cy.get('[data-cy=AnimalListSearchInput] input').type(animalNameBis);
    cy.get('[data-cy=AnimalEditBtn]').click();
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').clear();
    cy.get('[data-cy=AnimalFormAnimalNameInput] input').type(animalName);
    cy.get('[data-cy=AnimalFormAnimalIdInput] input').type(animalName);

    cy.get('[data-cy=AnimalFormOkBtn]').click();
    cy.get('[data-cy=AnimalListSearchInput] input').clear();

    cy.get('[data-cy=ReturnFlightHeaderBtn]').click();
    cy.get('.v-dialog').contains(`Êtes-vous sûr de vouloir quitter la page sans enregistrer vos travaux ?`).should('exist');
    cy.get('[data-cy=ConfirmDialogCancelBtn]').click();
    cy.url().should('match', /vols\/\d+\/dossier\/animal/);

    cy.get('[data-cy=ReturnFlightHeaderBtn]').click();
    cy.get('.v-dialog').contains(`Êtes-vous sûr de vouloir quitter la page sans enregistrer vos travaux ?`).should('exist');
    cy.get('[data-cy=ConfirmDialogConfirmBtn]').click();
    cy.url().should('match', /vols/);
  });

  it(`On doit pouvoir exporter la liste courante sous format excel`, () => {
    goToAnimalPage();
    cy.get('[data-cy=AnimalImportExportBtn]').click();
    cy.verifyDownload('LISTE_ANIMAUX.xlsx');
  });

  // it(`On doit pouvoir télécharger le modèle de fichier`, () => {
  //   goToAnimalPage();
  //
  //   cy.get('[data-cy=AnimalDownloadFileBtn]').click();
  //   cy.verifyDownload('Modele_fichier_chien_militaire.xlsx');
  // });
});

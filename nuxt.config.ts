import colors from 'vuetify/util/colors';
import { fr } from 'vuetify/locale';

export default defineNuxtConfig({
  ssr: false,

  features: {
    inlineStyles: true,
  },

  vite: {
    build: {
      rollupOptions: {
        output: {
          experimentalMinChunkSize: Infinity,
        }
      }
    }
  },

  app: {
    rootAttrs: {
      id: 'app'
    },
    head: {
      title: 'Airvam',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'Airvam' },
        { name: 'format-detection', content: 'telephone=no' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ]
    }
  },

  css: ['assets/main.scss'],
  components: [
    { path: 'components/mixins', pathPrefix: false },
    '~/components',
  ],

  modules: [
    'vuetify-nuxt-module',
    '@sidebase/nuxt-auth',
    '@vee-validate/nuxt',
    '@pinia/nuxt',
    'nuxt-security',
    '@radya/nuxt-dompurify',
  ],

  vuetify: {

    vuetifyOptions: {
      labComponents: 'VTimePicker',
      locale: {
        locale: 'fr',
        fallback: 'en',
        messages: { fr }
      },
      theme: {
        defaultTheme: 'light',
        themes: {
          light: {
            dark: false,
            colors: {
              primary: '#ffd36f',
              secondary: '#fe3089',
              accent: '#82B1FF',
              error: '#FF5252',
              info: '#2196F3',
              success: '#4CAF50',
              warning: '#FB8C00',
              background: '#FBFBFC'
            },
          },
          dark: {
            dark: true,
            colors: {
              primary: '#ffd36f',
              accent: '#fe3089',
              secondary: colors.amber.darken3,
              info: colors.teal.lighten1,
              warning: colors.amber.base,
              error: colors.deepOrange.accent4,
              success: colors.green.accent3,
              // background: colors.black.base
            }
          }
        },
      },
      icons: {
        defaultSet: 'md',
      }
    }
  },

  auth: {
    baseURL: import.meta.env.API_URL,
    provider: {
      type: 'local',
      endpoints: {
        signIn: { path: 'auth/local-login', method: 'post' },
        getSession: { path: 'user/me', method: 'get' },
        signOut: false,
      },
      session: {
        dataType: {
          email: 'string',
          firstName: 'string',
          lastName: 'string',
          rank: 'Rank',
          role: 'UserRole',
          unit: 'Unit',
          disabled: 'boolean',
        },
      },
    },
    globalAppMiddleware: {
      isEnabled: true,
    }
  },

  security: {
    headers: {
      contentSecurityPolicy: {
        'default-src': ["'self'"],
        'script-src': ["'self'", "'unsafe-inline'", "'unsafe-eval'"],
        'style-src': ["'self'", "'unsafe-inline'", 'https:'],
        'img-src': ["'self'", 'data:', 'https:'],
        'connect-src': ["'self'", import.meta.env.API_URL.split("/api")[0]],
      },
      xXSSProtection: '1; mode=block'
    }
  },

  runtimeConfig: {
    public: {
      appEnv: import.meta.env.APP_ENV,

      remote: {
        baseURL: import.meta.env.API_URL,
      }
    }
  },

  devServer: {
    port: 4200 // default: 3000
  },

  compatibilityDate: '2024-08-22',
})
